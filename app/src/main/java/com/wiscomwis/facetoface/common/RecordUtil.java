package com.wiscomwis.facetoface.common;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.text.TextUtils;
import android.widget.ImageView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.library.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 录音工具类
 * Created by zhangdroid on 2017/6/27.
 */
public class RecordUtil {
    private static volatile RecordUtil sDefault;
    // 录音器
    private MediaRecorder mRecorder;
    // 播放器
    private MediaPlayer mPlayer;
    private  AnimationDrawable animationDrawable;
    private  AnimationDrawable animationDrawable3;
    private ImageView iv;
    private ImageView iv3;
    private RecordUtil() {
    }

    public static RecordUtil getInstance() {
        if (sDefault == null) {
            synchronized (RecordUtil.class) {
                if (sDefault == null) {
                    sDefault = new RecordUtil();
                }
            }
        }
        return sDefault;
    }

    /**
     * 开始录音
     *
     * @param path 录音后输出的文件路径，绝对路径
     */
    public void startRecord(String path) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        File file = new File(path);
//        if (!file.getParentFile().exists()) {
//            file.getParentFile().mkdirs();
//        }
        // 初始化MediaRecorder
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
        }

        try {
            // 设置录音源为麦克风
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            //采样率
//            mRecorder.setAudioSamplingRate(44100);
            // 设置录音文件格式(顺序不能换)
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            // 设置录音文件存放路径
            mRecorder.setOutputFile(path);
            // 开始录音
            mRecorder.prepare();
            mRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止录音
     */
    public void stopRecord() {
        try {
            if (mRecorder != null) {
                mRecorder.stop();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放录音器资源
     */
    public void releaseRecord() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
    }

    /**
     * * 开始播放录音
     *
     * @param path     录音文件路径（绝对路径）
     * @param listener 监听器，控制动画的播放
     */
    public void play(String path, final OnPlayerListener listener) {
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
        }
        try {
            mPlayer.reset();
            mPlayer.setDataSource(path);
            mPlayer.prepare();
            mPlayer.start();

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                    }
                }
            });
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            mPlayer.release();
            mPlayer = null;
            if (listener != null) {
                listener.onCompleted();
            }
            e.printStackTrace();
        }
    }

    public void play2(String path, final ImageView iv2, Context context, final OnPlayerListener listener) {
        iv=iv2;
        List<Drawable> list = new ArrayList<Drawable>();
        list.add(context.getDrawable(R.drawable.sound_wave_left1));
        list.add(context.getDrawable(R.drawable.sound_wave_left2));
        list.add(context.getDrawable(R.drawable.sound_wave_left3));
        animationDrawable = Utils.getFrameAnim(list, true, 200);
        iv.setImageDrawable(animationDrawable);
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
        }
        try {
            mPlayer.reset();
            mPlayer.setDataSource(path);
            mPlayer.prepare();
            mPlayer.start();
            if(animationDrawable3!=null){
                if(animationDrawable3.isRunning()){
                    animationDrawable3.stop();
                    animationDrawable.start();
                    if (iv3!=null) {
                        iv3.setImageResource(R.drawable.sound_wave_left3);
                    }
                } else{
                    animationDrawable.start();
                }
            }else{
                animationDrawable.start();
            }
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                        if(animationDrawable!=null){
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        if(animationDrawable3!=null){
                            if (animationDrawable3.isRunning()) {
                                animationDrawable3.stop();
                            }
                        }
                        if (iv!=null) {
                            iv.setImageResource(R.drawable.sound_wave_left3);
                        }
                        if(iv3!=null){
                            iv3.setImageResource(R.drawable.sound_wave_left3);
                        }
                    }
                }
            });
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                        if(animationDrawable!=null){
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        if(animationDrawable3!=null){
                            if (animationDrawable3.isRunning()) {
                                animationDrawable3.stop();
                            }
                        }
                        if (iv!=null) {
                            iv.setImageResource(R.drawable.sound_wave_left3);
                        }
                        if(iv3!=null){
                            iv3.setImageResource(R.drawable.sound_wave_left3);
                        }
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            mPlayer.release();
            mPlayer = null;
            if (listener != null) {
                listener.onCompleted();
                if(animationDrawable!=null){
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                }
                if(animationDrawable3!=null){
                    if (animationDrawable3.isRunning()) {
                        animationDrawable3.stop();
                    }
                }
                if (iv!=null) {
                    iv.setImageResource(R.drawable.sound_wave_left3);
                }
                if(iv3!=null){
                    iv3.setImageResource(R.drawable.sound_wave_left3);
                }
            }
            e.printStackTrace();
        }
    }
    public void play3(String path, final ImageView iv2, Context context, final OnPlayerListener listener) {
        iv3=iv2;
        List<Drawable> list = new ArrayList<Drawable>();
        list.add(context.getDrawable(R.drawable.sound_wave_left1));
        list.add(context.getDrawable(R.drawable.sound_wave_left2));
        list.add(context.getDrawable(R.drawable.sound_wave_left3));
        animationDrawable3 = Utils.getFrameAnim(list, true, 200);
        iv3.setImageDrawable(animationDrawable3);
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
        }
        try {
            mPlayer.reset();
            mPlayer.setDataSource(path);
            mPlayer.prepare();
            mPlayer.start();
            if(animationDrawable!=null){
                if(animationDrawable.isRunning()){
                    animationDrawable.stop();
                    if (iv!=null) {
                        iv.setImageResource(R.drawable.sound_wave_left3);
                    }
                    animationDrawable3.start();
                } else{
                    animationDrawable3.start();
                }
            }else{
                animationDrawable3.start();
            }
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                        if(animationDrawable!=null){
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        if(animationDrawable3!=null){
                            if (animationDrawable3.isRunning()) {
                                animationDrawable3.stop();
                            }
                        }
                        if (iv!=null) {
                            iv.setImageResource(R.drawable.sound_wave_left3);
                        }
                        if(iv3!=null){
                            iv3.setImageResource(R.drawable.sound_wave_left3);
                        }
                    }
                }
            });
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mPlayer.release();
                    mPlayer = null;
                    if (listener != null) {
                        listener.onCompleted();
                        if(animationDrawable!=null){
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        if(animationDrawable3!=null){
                            if (animationDrawable3.isRunning()) {
                                animationDrawable3.stop();
                            }
                        }
                        if (iv!=null) {
                            iv.setImageResource(R.drawable.sound_wave_left3);
                        }
                        if(iv3!=null){
                            iv3.setImageResource(R.drawable.sound_wave_left3);
                        }
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            mPlayer.release();
            mPlayer = null;
            if (listener != null) {
                listener.onCompleted();
                if(animationDrawable!=null){
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                }
                if(animationDrawable3!=null){
                    if (animationDrawable3.isRunning()) {
                        animationDrawable3.stop();
                    }
                }
                if (iv!=null) {
                    iv.setImageResource(R.drawable.sound_wave_left3);
                }
                if(iv3!=null){
                    iv3.setImageResource(R.drawable.sound_wave_left3);
                }
            }
            e.printStackTrace();
        }
    }

    /**
     * 停止播放录音
     */
    public void stop() {
        if (mPlayer != null) {
            mPlayer.stop();
            animationDrawable.stop();
            if(animationDrawable!=null){
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
            }
            if(animationDrawable3!=null){
                if (animationDrawable3.isRunning()) {
                    animationDrawable3.stop();
                }
            }
            if (iv!=null) {
                iv.setImageResource(R.drawable.sound_wave_left3);
            }
            if(iv3!=null){
                iv3.setImageResource(R.drawable.sound_wave_left3);
            }
        }
    }

    /**
     * 释放播放器资源
     */
    public void release() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
            if(animationDrawable!=null){
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
            }
            if(animationDrawable3!=null){
                if (animationDrawable3.isRunning()) {
                    animationDrawable3.stop();
                }
            }
            if (iv!=null) {
                iv.setImageResource(R.drawable.sound_wave_left3);
            }
            if(iv3!=null){
                iv3.setImageResource(R.drawable.sound_wave_left3);
            }
        }
    }

    /**
     * 是否正在播放
     *
     * @return
     */
    public boolean isPlaying() {
        return !(mPlayer == null || !mPlayer.isPlaying());
    }

    /**
     * 切换播放/暂停状态
     *
     * @param path     录音文件路径（绝对路径）
     * @param listener 监听器，控制动画的播放
     */
    public void toggle(String path, OnPlayerListener listener) {
        try {
            if (isPlaying()) {
                // 暂停
                mPlayer.pause();
                if (listener != null) {
                    listener.onPaused();
                }
            } else {
                // 播放
                play(path, listener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放器状态监听器
     */
    public interface OnPlayerListener {

        /**
         * 播放完成/异常
         */
        void onCompleted();

        /**
         * 暂停
         */
        void onPaused();
    }
    public void play4(String path, final ImageView iv2, Context context, final OnPlayerListener listener) {
        //其他条目,释放资源
        if (animationDrawable != null) {
            animationDrawable.selectDrawable(2);//恢复到第一帧
            animationDrawable.stop();
        }
        RecordUtil.getInstance().release();

        //点击条目开始播放动画和录音
        startVoiceAnimation(iv2, context);
        // 播放动画
        if (animationDrawable != null) {
            animationDrawable.start();
        }
        RecordUtil.getInstance().play(path, new RecordUtil.OnPlayerListener() {
            @Override
            public void onCompleted() {//播放完成，停止动画
                animationDrawable.stop();
//                iv2.setImageResource(R.drawable.sound_wave_left3);
                animationDrawable.selectDrawable(2);
            }

            @Override
            public void onPaused() {
                animationDrawable.stop();
//                iv2.setImageResource(R.drawable.sound_wave_left3);
                animationDrawable.selectDrawable(2);
            }
        });
    }
    private void startVoiceAnimation(ImageView iv_sound_voice, Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left1));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left2));
        drawableList.add(context.getResources().getDrawable(R.drawable.sound_wave_left3));
        animationDrawable = Utils.getFrameAnim(drawableList, true, 200);
        iv_sound_voice.setImageDrawable(animationDrawable);
    }
}

