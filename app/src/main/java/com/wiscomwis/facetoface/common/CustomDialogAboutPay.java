package com.wiscomwis.facetoface.common;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.AlipayInfo;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.BlockBean;
import com.wiscomwis.facetoface.data.model.PayDict;
import com.wiscomwis.facetoface.data.model.PayWay;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.facetoface.data.model.WeChatInfo;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.PayPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.FinishVideoToChatActivityEvent;
import com.wiscomwis.facetoface.event.FinishWxActivity;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.ReportParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.ReportActivity;
import com.wiscomwis.facetoface.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.facetoface.ui.dialog.adapter.DialogKeyAndDiamondsAdapter;
import com.wiscomwis.facetoface.ui.pay.alipay.AlipayHelper;
import com.wiscomwis.facetoface.wxapi.WXPayEntryActivity;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/9/5.
 */

public class CustomDialogAboutPay {
    private static int ALIPAYANDWEIXINFILE = 1;//1微信,2支付宝
    private static String PAYFIELD = null;
    private static int pos = 0;
    private static int vippos =2;
    private static int keypos = 0;
    private static int diamondspos = 0;
    private static int diamondspos2 = 0;
    //举报对话框
    public static void reportShow(final Context context, final String userId) {

        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_report, null);
        bottomDialog.setContentView(contentView);
        RelativeLayout rl_report = (RelativeLayout) contentView.findViewById(R.id.dialog_rl_report);
        RelativeLayout rl_lahei = (RelativeLayout) contentView.findViewById(R.id.dialog_rl_lahei);
        final TextView tv_block = (TextView) contentView.findViewById(R.id.tv_block);
        rl_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                LaunchHelper.getInstance().launch(context, ReportActivity.class, new ReportParcelable(userId));
            }
        });
        ApiManager.getIsBlock(userId, new IGetDataListener<BlockBean>() {
            @Override
            public void onResult(BlockBean blockBean, boolean isEmpty) {
                if (blockBean != null) {
                    if (blockBean.getIsBlock() == 0) {//未拉黑
                        tv_block.setText(context.getString(R.string.block));
                    } else {//已拉黑
                        tv_block.setText(context.getString(R.string.block_cancel));
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                tv_block.setText(context.getString(R.string.block));
            }
        });


        rl_lahei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_block.getText().toString().trim().equals(context.getString(R.string.block))) {
                    Toast.makeText(context, context.getString(R.string.block_success), Toast.LENGTH_SHORT).show();
                    bottomDialog.dismiss();
                    ApiManager.lahei(userId, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                } else {
                    Toast.makeText(context, context.getString(R.string.block_cancel_success), Toast.LENGTH_SHORT).show();
                    bottomDialog.dismiss();
                    ApiManager.cancellahei(userId, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                }
            }
        });
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    private static void setRecyclerView(Context context, RecyclerView recyelerview) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyelerview.setLayoutManager(gridLayoutManager);
        recyelerview.setHasFixedSize(true);
    }

    //购买很多钻石
    public static void purchaseDiamondShow(final Context context,int paySource) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_purchase_diamonds, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_dismiss = (ImageView) contentView.findViewById(R.id.purchase_diamonds_iv_dismiss);
        TextView tv_diamonds = (TextView) contentView.findViewById(R.id.purchase_diamonds_tv_diamonds);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.purchase_diamonds_recyclerview);
        final RelativeLayout rl_weixin = (RelativeLayout) contentView.findViewById(R.id.purchase_diamonds_rl_weixin);
        final RelativeLayout rl_alipay = (RelativeLayout) contentView.findViewById(R.id.purchase_diamonds_rl_alipay);
        Button btn_sure = (Button) contentView.findViewById(R.id.purchase_diamonds_btn_sure);
        setRecyclerView(context, recyclerView);
        tv_diamonds.setText(PayPreference.getDionmadsNum() + "");
        PayWay payWay = new Gson().fromJson(PayPreference.getDiamondInfo(), PayWay.class);
        final List<PayDict> dictPayList = payWay.getDictPayList();
        if (dictPayList != null && dictPayList.size() > 0) {
            diamondspos = 0;
            PAYFIELD = dictPayList.get(0).getServiceId();
        }
        ALIPAYANDWEIXINFILE = 1;
        final DialogKeyAndDiamondsAdapter adapter = new DialogKeyAndDiamondsAdapter(context, R.layout.dialog_purchase_diamonds_item, dictPayList);
        adapter.setIsDiamonds();//设置是钻石
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                adapter.setSelectedPosition(position);
                adapter.notifyDataSetChanged();
                diamondspos = position;
                PAYFIELD = dictPayList.get(position).getServiceId();
            }
        });
        cancelDialog(bottomDialog, iv_dismiss);
        payClick(rl_weixin, rl_alipay);
       paySureCommit(context, bottomDialog, btn_sure, dictPayList, 1,paySource);
        dialogShow(context, bottomDialog, contentView);
    }

    //批量打招呼
    public static void sayHelloShow(final Context context, List<SearchUser> searchUserList1) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_say_hello, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.setCancelable(true);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.say_hello_recyclerview);
        Button btn_sure = (Button) contentView.findViewById(R.id.say_hello_btn_sure);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 5);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        AlbumPhotoAdapter adapter = new AlbumPhotoAdapter(context, R.layout.album_recyclerview_item, 0);
        recyclerView.setAdapter(adapter);
        final List<UserPhoto> list = new ArrayList<>();
        final List<String> listUser = new ArrayList<>();
        if (searchUserList1 != null && searchUserList1.size() > 0) {
            for (int i = 0; i < searchUserList1.size(); i++) {
                UserPhoto userPhoto = new UserPhoto();
                SearchUser searchUser = searchUserList1.get(i);
                if (searchUser != null) {
                    listUser.add(String.valueOf(searchUser.getUesrId()));
                    userPhoto.setFileUrlMinimum(searchUser.getIconUrlMininum());
                    list.add(userPhoto);
                    adapter.bind(list);
                }
            }
        }
        dialogShow2(context, bottomDialog, contentView);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, context.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < listUser.size(); i++) {
                    sb.append(listUser.get(i)).append(",");
                }
                String s = sb.toString();
                if (!TextUtils.isEmpty(s)) {
                    String substring = s.substring(0, s.length() - 1);
                    ApiManager.sayHelloGroup(substring, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {

                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }


                bottomDialog.dismiss();

            }
        });
    }


    //两个钻石的支付
    public static void diamondRadiousShow(final Context context) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_slight_diamonds, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_dismiss = (ImageView) contentView.findViewById(R.id.dialog_diamond_iv_dismiss);
        final RelativeLayout rl_100 = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_100);
        final RelativeLayout rl_600 = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_600);
        final RelativeLayout rl_weixin = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_weixin);
        final RelativeLayout rl_alipay = (RelativeLayout) contentView.findViewById(R.id.dialog_diamond_rl_alipay);
        Button btn_sure = (Button) contentView.findViewById(R.id.dialog_diamond_btn_sure);
//        PayWay payWay = new Gson().fromJson(PayPreference.getVipInfo(), PayWay.class);
//        List<PayDict> dictPayList = payWay.getDictPayList();
//        PAYFIELD=dictPayList.get(0).getServiceId();
        cancelDialog(bottomDialog, iv_dismiss);
        rl_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDiamondDrawable(rl_100, rl_600, true);
                diamondspos2 = 0;
            }

        });
        rl_600.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDiamondDrawable(rl_100, rl_600, false);
                diamondspos2 = 1;
            }
        });
        payClick(rl_weixin, rl_alipay);
//         paySureCommit(context, bottomDialog, btn_sure,dictPayList.get(diamondspos2).getServiceName(),dictPayList.get(diamondspos2).getPrice(),3);
        dialogShow(context, bottomDialog, contentView);
    }

    //开通VIP
    public static void dregeVipShow(Context context,int paySource) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_dredge_vip, null);
        bottomDialog.setContentView(contentView);
        ImageView iv_dismiss = (ImageView) contentView.findViewById(R.id.dredge_vip_iv_dismiss);
        final RelativeLayout rl_69= (RelativeLayout) contentView.findViewById(R.id.dredge_vip_rl_69);
        final RelativeLayout rl_100 = (RelativeLayout) contentView.findViewById(R.id.dredge_vip_rl_100);
        final RelativeLayout rl_129 = (RelativeLayout) contentView.findViewById(R.id.dredge_vip_rl_129);
        TextView tv_servicename1 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_servicename1);
        TextView tv_servicename2 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_servicename2);
        TextView tv_servicename3 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_servicename3);
        TextView tv_price1 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_price1);
        TextView tv_price2 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_price2);
        TextView tv_price3 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_price3);
        TextView tv_des3 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_des3);
        TextView tv_des2 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_des2);
        TextView tv_des1 = (TextView) contentView.findViewById(R.id.dredge_vip_tv_des1);
        final RelativeLayout rl_weixin = (RelativeLayout) contentView.findViewById(R.id.dredge_vip_rl_weixin);
        final RelativeLayout rl_alipay = (RelativeLayout) contentView.findViewById(R.id.dredge_vip_rl_alipay);
        PayWay payWay = new Gson().fromJson(PayPreference.getVipInfo(), PayWay.class);
        final List<PayDict> dictPayList = payWay.getDictPayList();
        final int vipSaleCutdown = payWay.getVipSaleCutdown();
        if (dictPayList != null && dictPayList.size() > 0) {
            PAYFIELD = dictPayList.get(2).getServiceId();
            tv_servicename1.setText(dictPayList.get(0).getServiceName()+ context.getString(R.string.day)+"VIP");
            tv_price1.setText("￥2.30");
            tv_des1.setText("￥" + dictPayList.get(0).getPrice());
            tv_servicename2.setText(dictPayList.get(1).getServiceName() + context.getString(R.string.day)+"VIP");
            tv_price2.setText("￥1.11");
            tv_des2.setText("￥" + dictPayList.get(1).getPrice()+"|"+dictPayList.get(1).getServiceDesc());
            if(vipSaleCutdown>0){
                tv_servicename3.setText(dictPayList.get(3).getServiceName()+"VIP");
                tv_price3.setText(context.getString(R.string.vip_for_life));
                tv_des3.setText(dictPayList.get(3).getServiceDesc());
            }else{
                tv_servicename3.setText(dictPayList.get(2).getServiceName()+"VIP");
                tv_price3.setText(context.getString(R.string.vip_for_life));
                tv_des3.setText("￥" + dictPayList.get(1).getPrice()+"|"+dictPayList.get(2).getServiceDesc());
            }

        }
        ALIPAYANDWEIXINFILE = 1;
        Button btn_sure = (Button) contentView.findViewById(R.id.dredge_vip_btn_sure);
        cancelDialog(bottomDialog, iv_dismiss);
        rl_69.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDrawablethreeStatus(rl_69, rl_100,rl_129,0);
                vippos = 0;
                PAYFIELD = dictPayList.get(0).getServiceId();
            }
        });
        rl_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDrawablethreeStatus(rl_69, rl_100,rl_129,1);
                vippos = 1;
                PAYFIELD = dictPayList.get(1).getServiceId();
            }
        });
        rl_129.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vipSaleCutdown>0) {
                    setDrawablethreeStatus(rl_69, rl_100,rl_129,3);
                    vippos = 3;
                    PAYFIELD = dictPayList.get(3).getServiceId();
                }else{
                    setDrawablethreeStatus(rl_69, rl_100,rl_129,2);
                    vippos = 2;
                    PAYFIELD = dictPayList.get(2).getServiceId();
                }

            }
        });
        payClick(rl_weixin, rl_alipay);
        PayDict payDict = dictPayList.get(vippos);
        if (payDict != null) {
            paySureCommit(context, bottomDialog, btn_sure, dictPayList, 2,paySource);
        }
        dialogShow(context, bottomDialog, contentView);
    }

    //消费成功的提醒
    public static void paySucceedShow(final Context context, String serviceName, String price, int type,final int paySource) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_pay_succed, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        final TextView tv_keys = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney);
        final TextView tv_fuwu = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_fuwu);
        final TextView tv_yuan = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_yuan);
        final TextView tv_time = (TextView) contentView.findViewById(R.id.pay_succed_tv_howmaney_time);
        Button btn_sure = (Button) contentView.findViewById(R.id.pay_succed_btn_sure);
        tv_yuan.setText(context.getString(R.string.money) + price + context.getString(R.string.money));
        if (type == 1) {//钻石
            tv_keys.setText(serviceName);
            tv_fuwu.setText(context.getString(R.string.number_diamonds));
            PayPreference.saveDionmadsNum(PayPreference.getDionmadsNum() + Integer.parseInt(serviceName));
        } else if (type == 2) {//vip
            tv_keys.setText(serviceName);
            tv_fuwu.setText("VIP");
            UserPreference.setIsVip(true);
        }
        tv_time.setText(DateTimeUtil.convertTimeMillis2String(System.currentTimeMillis()));
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        dialogShow(context, bottomDialog, contentView);
    }

    //消费失败的提醒
    public static void payFaildShow(final Context context, final int type) {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_pay_faild, null);
        bottomDialog.setContentView(contentView);
        final TextView tv_lianxi = (TextView) contentView.findViewById(R.id.pay_faild_tv_lianxi);
        Button btn_sure = (Button) contentView.findViewById(R.id.pay_faild_btn_sure);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type==2){
                    String videoCallAccount = DataPreference.getVideoCallAccount();
                    String videoCallId = DataPreference.getVideoCallId();
                    String videoCallImageUrl = DataPreference.getVideoCallImageUrl();
                    String videoCallName = DataPreference.getVideoCallName();
                    if (!videoCallAccount.equals("0")) {
                        EventBus.getDefault().post(new FinishVideoToChatActivityEvent(Long.parseLong(videoCallId),videoCallAccount,videoCallName,videoCallImageUrl,0));
                    }
                }
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        tv_lianxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchHelper.getInstance().launch(context, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", context.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
                bottomDialog.dismiss();
                EventBus.getDefault().post(new FinishWxActivity());
            }
        });
        dialogShow(context, bottomDialog, contentView);
    }


    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //任意点击一下对话框消失的情况
    private static void dialogShow2(Context context, Dialog bottomDialog, View contentView) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    //设置选中与未选中状态
    private static void setDrawable(RelativeLayout view1, RelativeLayout view2, boolean flag) {
        if (flag) {
            view1.setBackgroundResource(R.drawable.all_pay_selected);
            view2.setBackgroundResource(R.drawable.all_pay_un_selected);
        } else {
            view2.setBackgroundResource(R.drawable.all_pay_selected);
            view1.setBackgroundResource(R.drawable.all_pay_un_selected);
        }
    }
    //设置选中与未选中状态 三种按钮
    private static void setDrawablethreeStatus(RelativeLayout view1, RelativeLayout view2,RelativeLayout view3,int flag) {
        switch (flag){
            case 0:
                view1.setBackgroundResource(R.drawable.vip_big_kuang_icon);
                view2.setBackgroundResource(0);
                view3.setBackgroundResource(0);
                break;
            case 1:
                view1.setBackgroundResource(0);
                view2.setBackgroundResource(R.drawable.vip_big_kuang_icon);
                view3.setBackgroundResource(0);
                break;
            case 2:
                view1.setBackgroundResource(0);
                view2.setBackgroundResource(0);
                view3.setBackgroundResource(R.drawable.vip_big_kuang_icon);
                break;
            default:
                view1.setBackgroundResource(0);
                view2.setBackgroundResource(0);
                view3.setBackgroundResource(R.drawable.vip_big_kuang_icon);
                break;
        }
    }

    //设置两个钻石的图片
    private static void setDiamondDrawable(RelativeLayout view1, RelativeLayout view2, boolean flag) {
        if (flag) {
            view1.setBackgroundResource(R.drawable.purchase_diamond_selected);
            view2.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
        } else {
            view2.setBackgroundResource(R.drawable.purchase_diamond_selected);
            view1.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
        }
    }

    //取消对话框的显示
    private static void cancelDialog(final Dialog bottomDialog, ImageView iv_dismiss) {
        iv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
    }

    //选择支付的方式
    private static void payClick(final RelativeLayout rl_weixin, final RelativeLayout rl_alipay) {
        rl_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDrawable(rl_weixin, rl_alipay, true);
                ALIPAYANDWEIXINFILE = 1;
            }
        });
        rl_alipay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDrawable(rl_weixin, rl_alipay, false);
                ALIPAYANDWEIXINFILE = 2;
            }
        });
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }

    //最后确认提交并调用支付
    private static void paySureCommit(final Context context, final Dialog bottomDialog, Button btn_sure, final List<PayDict> dictPayList, final int type,final int paySource) {
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                if (ALIPAYANDWEIXINFILE == 2) {//支付宝
                    ApiManager.alipayOrderInfo(UserPreference.getId(), PAYFIELD, new IGetDataListener<AlipayInfo>() {
                        @Override
                        public void onResult(AlipayInfo baseModel, boolean isEmpty) {
                            if (baseModel != null) {
//                                EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);//沙盒测试 上线时关闭
                                if (type == 1) {
                                    AlipayHelper.getInstance().callAlipayApi(context, baseModel.getAliPayOrder(), dictPayList.get(diamondspos).getServiceName(), dictPayList.get(diamondspos).getPrice(), type,paySource);
                                } else if (type == 2) {
                                    AlipayHelper.getInstance().callAlipayApi(context, baseModel.getAliPayOrder(), dictPayList.get(vippos).getServiceName(), dictPayList.get(vippos).getPrice(), type,paySource);
                                } else if (type == 3) {
                                    AlipayHelper.getInstance().callAlipayApi(context, baseModel.getAliPayOrder(), dictPayList.get(keypos).getServiceName(), dictPayList.get(keypos).getPrice(), type,paySource);
                                }
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                } else {//微信
                    ApiManager.wxOrderInfo(UserPreference.getId(), PAYFIELD, new IGetDataListener<WeChatInfo>() {
                        @Override
                        public void onResult(WeChatInfo weChatInfo, boolean isEmpty) {
                            if (weChatInfo != null) {
                                Intent intent = new Intent(context, WXPayEntryActivity.class);
                                if (type == 1) {
                                    String serverName = dictPayList.get(diamondspos).getServiceName();
                                    String price = dictPayList.get(diamondspos).getPrice();
                                    intent.putExtra("type", 1);
                                    intent.putExtra("serviceName", serverName);
                                    intent.putExtra("price", price);
                                    intent.putExtra("paySource",paySource);
                                } else if (type == 2) {
                                    String serverName = dictPayList.get(vippos).getServiceName();
                                    String price = dictPayList.get(vippos).getPrice();
                                    intent.putExtra("type", 2);
                                    intent.putExtra("serviceName", serverName);
                                    intent.putExtra("price", price);
                                    intent.putExtra("paySource",paySource);
                                } else if (type == 3) {
                                    String serverName = dictPayList.get(keypos).getServiceName();
                                    String price = dictPayList.get(keypos).getPrice();
                                    intent.putExtra("type", 3);
                                    intent.putExtra("serviceName", serverName);
                                    intent.putExtra("price", price);
                                    intent.putExtra("paySource",paySource);
                                }
                                intent.putExtra("orderInf", weChatInfo.getWeixinPayOrder());
                                context.startActivity(intent);
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }
            }
        });
    }
//    private static void setMsgFlag(String extendTag){
//        ApiManager.userActivityTag(UserPreference.getId(), "", "16", extendTag, new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//            }
//        });
//    }
}
