package com.wiscomwis.facetoface.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 首页列表需要传递的参数
 * Created by zhangdroid on 2017/6/2.
 */
public class ListParcelable implements Parcelable {
    public int type;// 需要显示的列表类型

    public ListParcelable(int type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
    }

    protected ListParcelable(Parcel in) {
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<ListParcelable> CREATOR = new Parcelable.Creator<ListParcelable>() {
        @Override
        public ListParcelable createFromParcel(Parcel source) {
            return new ListParcelable(source);
        }

        @Override
        public ListParcelable[] newArray(int size) {
            return new ListParcelable[size];
        }
    };
}
