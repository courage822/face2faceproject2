package com.wiscomwis.facetoface.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by xuzhaole on 2018/3/13.
 */

public class VideoShowParcelable implements Parcelable {

    private String duration;
    private String videoFilePaht;
    private String avatarFilePath;

    public VideoShowParcelable(String duration, String videoFilePaht, String avatarFilePath) {
        this.duration = duration;
        this.videoFilePaht = videoFilePaht;
        this.avatarFilePath = avatarFilePath;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVideoFilePaht() {
        return videoFilePaht;
    }

    public void setVideoFilePaht(String videoFilePaht) {
        this.videoFilePaht = videoFilePaht;
    }

    public String getAvatarFilePath() {
        return avatarFilePath;
    }

    public void setAvatarFilePath(String avatarFilePath) {
        this.avatarFilePath = avatarFilePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.duration);
        dest.writeString(this.videoFilePaht);
        dest.writeString(this.avatarFilePath);
    }

    protected VideoShowParcelable(Parcel in) {
        this.duration = in.readString();
        this.videoFilePaht = in.readString();
        this.avatarFilePath = in.readString();
    }

    public static final Creator<VideoShowParcelable> CREATOR = new Creator<VideoShowParcelable>() {
        @Override
        public VideoShowParcelable createFromParcel(Parcel source) {
            return new VideoShowParcelable(source);
        }

        @Override
        public VideoShowParcelable[] newArray(int size) {
            return new VideoShowParcelable[size];
        }
    };
}
