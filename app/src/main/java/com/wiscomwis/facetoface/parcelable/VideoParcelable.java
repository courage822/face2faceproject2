package com.wiscomwis.facetoface.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转视频聊天页面需要传递的参数
 * Created by zhangdroid on 2017/6/20.
 */
public class VideoParcelable implements Parcelable {
    /**
     * 是否被邀请
     */
    public boolean isInvited;
    /**
     * 用户id（对方）
     */
    public long guid;
    /**
     * 用户account（对方）
     */
    public String account;
    /**
     * 用户account（对方）
     */
    public String imageUrl;
    /**
     * 用户昵称（对方）
     */
    public String nickname;
    /**
     * 频道id
     */
    public String channelId;
    /**
     * 播主价格
     */
    public String hostPrice;
    /**
     * 呼叫的类型0为视频，1为语音
     */
    public int type;

    public VideoParcelable(long guid, String account, String imageUrl, String nickname,
                           String channelId, String hostPrice, int type,boolean isInvited) {
        this.guid = guid;
        this.account = account;
        this.imageUrl = imageUrl;
        this.nickname = nickname;
        this.channelId = channelId;
        this.hostPrice = hostPrice;
        this.type = type;
        this.isInvited = isInvited;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isInvited ? (byte) 1 : (byte) 0);
        dest.writeLong(this.guid);
        dest.writeString(this.account);
        dest.writeString(this.imageUrl);
        dest.writeString(this.nickname);
        dest.writeString(this.channelId);
        dest.writeString(this.hostPrice);
        dest.writeInt(this.type);
    }

    protected VideoParcelable(Parcel in) {
        this.isInvited = in.readByte() != 0;
        this.guid = in.readLong();
        this.account = in.readString();
        this.imageUrl = in.readString();
        this.nickname = in.readString();
        this.channelId = in.readString();
        this.hostPrice = in.readString();
        this.type = in.readInt();
    }

    public static final Creator<VideoParcelable> CREATOR = new Creator<VideoParcelable>() {
        @Override
        public VideoParcelable createFromParcel(Parcel source) {
            return new VideoParcelable(source);
        }

        @Override
        public VideoParcelable[] newArray(int size) {
            return new VideoParcelable[size];
        }
    };
}
