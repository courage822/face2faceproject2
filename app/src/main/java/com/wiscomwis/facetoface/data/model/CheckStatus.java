package com.wiscomwis.facetoface.data.model;

/**
 * Created by WangYong on 2018/2/3.
 */

public class CheckStatus extends BaseModel {
    private int auditStatus;//0、审核中 ,1、审核通过 ,-1 审核失败 ,-2未提交审核
    private int showStatus;//审核中下判断  1 完整  2 不完整


    public CheckStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(int showStatus) {
        this.showStatus = showStatus;
    }


    @Override
    public String toString() {
        return "CheckStatus{" +
                "auditStatus=" + auditStatus +
                '}';
    }
}
