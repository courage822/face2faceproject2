package com.wiscomwis.facetoface.data.model;

/**
 * 登录对象
 * Created by zhangdroid on 2017/5/24.
 */
public class Login extends BaseModel {
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private UserDetail userDetail;

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", userDetail=" + userDetail +
                '}';
    }
}
