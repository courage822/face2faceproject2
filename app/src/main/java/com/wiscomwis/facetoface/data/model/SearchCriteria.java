package com.wiscomwis.facetoface.data.model;

import java.util.List;

/**
 * 搜索条件对象
 * Created by zhangdroid on 2017/6/7.
 */
public class SearchCriteria {
    private String country;// 国家
    private List<String> spokenLanguage;// 语言

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getSpokenLanguage() {
        return spokenLanguage;
    }

    public void setSpokenLanguage(List<String> spokenLanguage) {
        this.spokenLanguage = spokenLanguage;
    }
}
