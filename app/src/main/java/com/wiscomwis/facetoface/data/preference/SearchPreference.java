package com.wiscomwis.facetoface.data.preference;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.data.model.SearchCriteria;
import com.wiscomwis.library.util.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 保存搜索条件
 * Created by zhangdroid on 2017/6/7.
 */
public class SearchPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private SearchPreference() {
    }

    public static final String NAME = "pre_search";
    /**
     * 国家
     */
    private static final String COUNTRY = "search_country";
    /**
     * 语言
     */
    private static final String LANGUAGE = "search_language";

    public static void saveSearchCountry(String country) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, COUNTRY, country);
    }

    public static void saveSearchLanguage(String language) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, LANGUAGE, language);
    }

    public static SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();
        // 设置用户注册时国家和语言为默认搜索条件
        searchCriteria.setCountry(SharedPreferenceUtil.getStringValue(mContext, NAME, COUNTRY,null));
        List<String> languageList = new ArrayList<>();
        String spokenLanguage = SharedPreferenceUtil.getStringValue(mContext, NAME, LANGUAGE, null);
        if (!TextUtils.isEmpty(spokenLanguage)) {
            String[] languageArray = spokenLanguage.split(",");
            if (languageArray.length > 0) {
                for (String language : languageArray) {
                    languageList.add(language);
                }
            }
        }
        searchCriteria.setSpokenLanguage(languageList);
        return searchCriteria;
    }

}
