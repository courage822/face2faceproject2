package com.wiscomwis.facetoface.data.model;

import java.util.List;

/**
 * Created by xuzhaole on 2017/11/23.
 */

public class HostAutoReplyBean extends BaseModel{

    private List<THostAutoReply> type1Reply;//自动回复(THostAutoReply)集合
    private List<THostAutoReply> type2Reply;//主动打招呼(THostAutoReply)集合

    private boolean showType1AddBtn;//是否显示添加自动回复按钮(boolean)
    private String msg;//提示语
    private boolean showType2AddBtn;//是否显示添加主动打招呼按钮(boolean)


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public List<THostAutoReply> getType2Reply() {
        return type2Reply;
    }

    public void setType2Reply(List<THostAutoReply> type2Reply) {
        this.type2Reply = type2Reply;
    }

    public void setShowType2AddBtn(boolean showType2AddBtn) {
        this.showType2AddBtn = showType2AddBtn;
    }

    public boolean isShowType2AddBtn() {
        return showType2AddBtn;
    }


    public List<THostAutoReply> getType1Reply() {
        return type1Reply;
    }

    public void setType1Reply(List<THostAutoReply> type1Reply) {
        this.type1Reply = type1Reply;
    }

    public boolean isShowType1AddBtn() {
        return showType1AddBtn;
    }

    public void setShowType1AddBtn(boolean showType1AddBtn) {
        this.showType1AddBtn = showType1AddBtn;
    }

    @Override
    public String toString() {
        return "HostAutoReplyBean{" +
                "type1Reply=" + type1Reply +
                ", type2Reply=" + type2Reply +
                ", showType1AddBtn=" + showType1AddBtn +
                ", msg='" + msg + '\'' +
                ", showType2AddBtn=" + showType2AddBtn +
                '}';
    }
}
