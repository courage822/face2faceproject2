package com.wiscomwis.facetoface.data.model;

import android.support.annotation.NonNull;

/**
 * Created by WangYong on 2017/9/12.
 */

public class AccountPwdInfo{
    private String account;
    private String pwd;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "AccountPwdInfo{" +
                "account='" + account + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
