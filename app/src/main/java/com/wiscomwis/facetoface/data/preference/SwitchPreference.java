package com.wiscomwis.facetoface.data.preference;

import android.content.Context;

import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.data.model.Switch;
import com.wiscomwis.library.util.SharedPreferenceUtil;

/**
 * 保存搜索条件
 * Created by zhangdroid on 2017/6/7.
 */
public class SwitchPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private SwitchPreference() {
    }
//    "allpaySwitch":{支付相关总开关}
//	"alipaySwitch":{支付宝支付开关}
//	"weixinpaySwitch":{微信支付开关}
//	"beanpaySwitch":{钻石开关}
//	"keypaySwitch":{钥匙开关}
//	"vippaySwitch":{vip开关}
//	"walletSwitch":{我的钱包开关}


    public static final String NAME = "switch";

    private static final String ALLAPYSWITCH = "allpaySwitch";//支付总开关
    private static final String ALIPAYSWITCH="alipaySwitch";//支付宝支付开关
    private static final String WEIXINPAYSWITCH="weixinpaySwitch";//微信支付开关
    private static final String BEANPAYSWITCH="beanpaySwitch";//钻石开关
    private static final String VIPPAYSWITCH="vippaySwitch";//vip开关
    private static final String WALLETSWITCH="walletSwitch";//我的钱包开关

    public static void saveSwitch(Switch swi){
        if(swi!=null){
            saveAllPay(swi.getAllpaySwitch());
            saveAlipay(swi.getAlipaySwitch());
            saveWeixinPay(swi.getWeixinpaySwitch());
            saveBeanPay(swi.getBeanpaySwitch());
            saveVipPay(swi.getVippaySwitch());
            saveWalletPay(swi.getWalletSwitch());
        }
    }
    private static void saveAllPay(int country) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, ALLAPYSWITCH, country);
    }

    private static void saveAlipay(int language) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, ALIPAYSWITCH, language);
    }
    private static void saveWeixinPay(int language) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, WEIXINPAYSWITCH, language);
    }
    private static void saveBeanPay(int language) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, BEANPAYSWITCH, language);
    }

    private static void saveVipPay(int language) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, VIPPAYSWITCH, language);
    }
    private static void saveWalletPay(int language) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, WALLETSWITCH, language);
    }
    //获取开关
    public static int getAllPay() {
       return SharedPreferenceUtil.getIntValue(mContext, NAME, ALLAPYSWITCH, -1);
    }

    public static int getAlipay() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, ALIPAYSWITCH, -1);
    }
    public static int getWeixinPay() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, WEIXINPAYSWITCH, -1);
    }
    public static int getBeanPay() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, BEANPAYSWITCH, -1);
    }

    public static int getVipPay() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, VIPPAYSWITCH, -1);
    }
    public static int getWalletPay() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, WALLETSWITCH, -1);
    }


}
