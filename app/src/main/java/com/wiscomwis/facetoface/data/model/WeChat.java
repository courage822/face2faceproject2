package com.wiscomwis.facetoface.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by WangYong on 2017/11/18.
 */

public class WeChat {
    /**
     * appid : wx184b0ee493d39249
     * noncestr : aa3c15c2093a47b296fe0173e5962837
     * package : Sign=WXPay
     * partnerid : 1489674462
     * prepayid : wx201711181124202cc37cce880185511262
     * sign : 99DDFDE4DAC0EEBB55BACEE1BDC8AA45
     * timestamp : 1510975462
     */

    private String appid;
    private String noncestr;
    @SerializedName("package")
    private String packageX;
    private String partnerid;
    private String prepayid;
    private String sign;
    private String timestamp;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getPackageX() {
        return packageX;
    }

    public void setPackageX(String packageX) {
        this.packageX = packageX;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
//    private String appid;
//    private String noncestr;
//    private String package;
//    private String partnerid;
//    private String prepayid;
//    private String sign;
//    private String timestamp;


}
