package com.wiscomwis.facetoface.ui.video.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by WangYong on 2017/11/28.
 */
public interface MassVideoOrVoiceContract {

    interface IView extends BaseView {
        /**
         * 获取发送的人数
         */
        void getSendNum(int num);
        /**
         * 获取发送的时间
         */
        void getSendTime(long time);
        /**
         * finish当前的activity
         */
        void finishActivity();
        /**
         *倒计时完成
         */
        void timeOnFinish();
        /**
         * 获取主播的价格
         */
         void getAnchorPrice(String price);
        /**
         * 可以点击挂断
         */
        void clickStop();
        /**
         * 是视频还是语音
         */
        void videoOrVoice(String voice);
        /**
         * 是否隐藏悬浮框
         */
        void isSeeFloat(boolean flag);
    }

    interface IPresenter extends BasePresenter {
        /**
         * 初始化数据
         */
        void initData(String type);
        /**
         * 开始倒计时
         */
        void startCountTime();
    }

}
