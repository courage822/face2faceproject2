package com.wiscomwis.facetoface.ui.pay.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.ui.pay.contract.IncomeContract;
import com.wiscomwis.facetoface.ui.pay.presenter.IncomeDetailPresenter;
import com.wiscomwis.library.util.SnackBarUtil;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/9/13.
 */

public class IncomeDetailFragment extends BaseFragment implements IncomeContract.IView {
    @BindView(R.id.fragment_incomedetail_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.fragment_incomedetail_xrecyerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_incomedetail_rl_root)
    RelativeLayout rl_root;
    @BindView(R.id.tv_error)
    TextView tv_error;
    private IncomeDetailPresenter mPresenter;
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_incomedetail;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefresh;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mSwipeRefresh.setColorSchemeResources(R.color.main_color);
        mSwipeRefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mPresenter = new IncomeDetailPresenter(this);
        mPresenter.start(mRecyclerView);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mPresenter.refresh();
                }
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(rl_root, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        mSwipeRefresh.setVisibility(View.GONE);
        tv_error.setVisibility(View.VISIBLE);
        tv_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwipeRefresh.setVisibility(View.VISIBLE);
                tv_error.setVisibility(View.GONE);
                mPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mPresenter.refresh();
            }
        });
    }

    @Override
    public void setAdapter(RecyclerView.Adapter pagerAdapter) {
        mRecyclerView.setAdapter(pagerAdapter);
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefresh && mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.refresh();
    }
}
