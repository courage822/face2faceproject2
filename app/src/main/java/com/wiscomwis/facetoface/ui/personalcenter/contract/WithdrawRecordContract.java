package com.wiscomwis.facetoface.ui.personalcenter.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.facetoface.ui.personalcenter.adapter.WithdrawRecordAdapter;

/**
 * Created by Administrator on 2017/7/14.
 */

public interface WithdrawRecordContract {

    interface IView extends BaseView {

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();

        WithdrawRecordAdapter getWithdrawRecordAdapter();
    }

    interface IPresenter extends BasePresenter {

        void loadHistoryList();

        void refresh();

        void loadMore();
    }
}
