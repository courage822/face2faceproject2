package com.wiscomwis.facetoface.ui.personalcenter.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

import java.io.File;

/**
 * Created by Administrator on 2017/6/16.
 */

public interface AuthenticationContract {
    interface IPresenter extends BasePresenter {
        /**
         * 跳转录制视频的界面
         */
        void goVideoRecordPage();
        /**
         * 获取用户的信息
         */
        void getUserInfo();
        /**
         * 开始录制
         */
        void startRecord();
        /**
         * 开始播放
         */
        void startPlay();
        /**
         * 重新录制
         */
        void reRecord();
        /**
         *获取facebook账号
         */
         void getFaceBookAccount();
        /**
         * 提交审核
         */
         void submitCheck(String videoNumber, File videoFile,File author);

    }
    interface IView extends BaseView {
        /**
         * 获取图片的路径
         */
        void getAvatarUrl(String url);
        void showLoading();

        void dismissLoading();

        void showNetworkError();
        /**
         * 设置facebook的账号
         */
        void setFaceBookAccount(String account);
        /**
         * 销毁Activity
         */
        void finishActivity();
        /**
         * 获取Facebook的账号
         */
        String copyFacebookAccount();
    }
}
