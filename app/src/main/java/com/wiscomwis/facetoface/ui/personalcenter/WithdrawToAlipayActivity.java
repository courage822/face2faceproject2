package com.wiscomwis.facetoface.ui.personalcenter;

import android.content.Intent;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.net.NetUtil;

import butterknife.BindView;

public class WithdrawToAlipayActivity extends BaseTopBarActivity implements View.OnClickListener {
    @BindView(R.id.tixian_detail_activity_iv_back)
    ImageView iv_back;
    @BindView(R.id.tixian_detail_activity_tv_desc)
    TextView tv_desc;
    @BindView(R.id.tixian_detail_activity_et_name)
    EditText et_name;
    @BindView(R.id.tixian_detail_activity_et_account)
    EditText et_account;
    @BindView(R.id.tixian_detail_activity_btn_save)
    Button btn_save;
    public static final int RESULT_CODE = 2001;

    private int isWeiXinOrAlipay;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdraw_to_alipay;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        isWeiXinOrAlipay = getIntent().getIntExtra("isWeiXinOrAlipay",1);
        if (isWeiXinOrAlipay == 2) {
            tv_desc.setText(getString(R.string.alipay_info));
            if (!TextUtils.isEmpty(UserPreference.getAlipayName()) && !TextUtils.isEmpty(UserPreference.getAlipayAccount())){
                et_name.setText(UserPreference.getAlipayName());
                et_account.setText(UserPreference.getAlipayAccount());
            }
        } else {
            tv_desc.setText(getString(R.string.withdraw_wechat_info));
            if (!TextUtils.isEmpty(UserPreference.getWechatName()) && !TextUtils.isEmpty(UserPreference.getWechatAccount())){
                et_name.setText(UserPreference.getWechatName());
                et_account.setText(UserPreference.getWechatAccount());
            }
        }
    }

    @Override
    protected void initViews() {
        et_name.addTextChangedListener(new MyTextWatch(et_name));
        et_account.addTextChangedListener(new MyTextWatch(et_account));
    }

    @Override
    protected void setListeners() {
        iv_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tixian_detail_activity_iv_back:
                finish();
                break;
            case R.id.tixian_detail_activity_et_name:
                btn_save.setEnabled(true);
                break;
            case R.id.tixian_detail_activity_et_account:
                btn_save.setEnabled(true);
                break;
            case R.id.tixian_detail_activity_btn_save:
                if (isWeiXinOrAlipay == 1){//微信
                    UserPreference.setWechatName(et_name.getText().toString().trim());
                    UserPreference.setWechatAccount(et_account.getText().toString().trim());
                }else{
                    UserPreference.setAlipayName(et_name.getText().toString().trim());
                    UserPreference.setAlipayAccount(et_account.getText().toString().trim());
                }
                finish();
                break;
        }
    }

    int flag = 0;

    public class MyTextWatch implements TextWatcher {
        private EditText EditId = null;

        public MyTextWatch(EditText id) {
            EditId = id;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (EditId == et_name) {
                if (TextUtils.isEmpty(et_name.getText()))
                    btn_save.setEnabled(false);
                else
                    flag = flag - 1;

            }
            if (EditId == et_account) {
                if (TextUtils.isEmpty(et_account.getText()))
                    btn_save.setEnabled(false);
                else
                    flag = flag - 1;

            }
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (EditId == et_name) {
                if (TextUtils.isEmpty(et_name.getText()))
                    btn_save.setEnabled(false);
                else
                    flag = flag + 1;

            }
            if (EditId == et_account) {
                if (TextUtils.isEmpty(et_account.getText()))
                    btn_save.setEnabled(false);
                else
                    flag = flag + 1;

            }
            if (flag == 2) {
                btn_save.setEnabled(true);
            }
        }
    }


}
