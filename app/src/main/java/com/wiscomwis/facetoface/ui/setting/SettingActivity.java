package com.wiscomwis.facetoface.ui.setting;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.ui.register.ChangePasswordActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 设置页面
 * Created by zhangdroid on 2017/5/12.
 */
public class SettingActivity extends BaseAppCompatActivity implements View.OnClickListener, SettingContract.IView {
    @BindView(R.id.setting_root)
    LinearLayout mLlRoot;
    @BindView(R.id.setting_switch)
    Switch mSwitch;
    @BindView(R.id.setting_clear_cache)
    RelativeLayout mRlClearCache;
    @BindView(R.id.setting_cache_size)
    TextView mTvCacheSize;
    @BindView(R.id.setting_logout)
    Button mBtnLogout;
    @BindView(R.id.setting_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.setting_about_us)
    RelativeLayout rl_about_us;
    @BindView(R.id.setting_rl_advice)
    RelativeLayout rl_advice;
    @BindView(R.id.setting_rl_change_password)
    RelativeLayout rl_change_password;
    private SettingPresenter mSettingPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }



    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mSettingPresenter = new SettingPresenter(this);
        if(TimeUtils.timeIsPast()){//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        }else if(!TimeUtils.timeIsPast()){
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSettingPresenter.toggleNoDistrub(isChecked);
            }
        });
        mRlClearCache.setOnClickListener(this);
        mBtnLogout.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_advice.setOnClickListener(this);
        rl_about_us.setOnClickListener(this);
        rl_change_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_clear_cache:// 清除缓存
                mSettingPresenter.clearCache();
                break;
            case R.id.setting_logout:// 登出
                mSettingPresenter.logout();
                break;
            case R.id.setting_activity_rl_back:
                finish();
                break;
            case R.id.setting_rl_advice:
                LaunchHelper.getInstance().launch(mContext,AdviceActivity.class);
                break;
            case R.id.setting_about_us:
                LaunchHelper.getInstance().launch(mContext,AbuoutUsActivity.class);
                break;
            case R.id.setting_rl_change_password:
                LaunchHelper.getInstance().launch(mContext,ChangePasswordActivity.class);
                break;
        }
    }

    @Override
    protected void loadData() {
        mSettingPresenter.getNoDistrubState();
        mSettingPresenter.getCacheSize();
        mSettingPresenter.getVersionName();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void toggleNoDistrub(boolean toggle) {
        mSwitch.setChecked(toggle);
    }

    @Override
    public void setCacheSize(String cacheSize) {
        mTvCacheSize.setText(cacheSize+"");
    }

    @Override
    public void setVersion(String versionName) {
//        mTvVersion.setText(getString(R.string.setting_version, versionName));
    }

    @Override
    public void showAlertDialog(String message, OnDialogClickListener listener) {
        AlertDialog.show(getSupportFragmentManager(), getString(R.string.alert), message,
                getString(R.string.positive), getString(R.string.negative), listener);
    }
    @Subscribe
    public void onEvent(SingleLoginFinishEvent event){
        finish();//单点登录销毁的activity
    }
    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();

    }
}
