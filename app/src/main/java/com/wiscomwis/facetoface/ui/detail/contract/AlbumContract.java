package com.wiscomwis.facetoface.ui.detail.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.facetoface.ui.detail.adapter.AlbumAdapter;
import com.wiscomwis.facetoface.ui.detail.adapter.NewAlbumAdapter;

/**
 * Created by Administrator on 2017/6/14.
 */
public interface AlbumContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        FragmentManager obtainFragmentManager();

        void setAdapter(NewAlbumAdapter adater);
    }

    interface IPresenter extends BasePresenter {
        void start(RecyclerView recyclerView);

        /**
         * 获取图片列表
         */
        void getPhotoInfo();

        /**
         * 上传图片
         */
        void uploadImage(int positon);

        /**
         * 删除照片
         */
        void updateImage(String picId, int positon);
    }

}
