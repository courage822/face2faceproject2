package com.wiscomwis.facetoface.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.ui.dialog.adapter.CountryAdapter;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.dialog.BaseDialogFragment;

/**
 * Created by Administrator on 2017/7/29.
 * 选择州的对话框
 */

public class StateSelectDialog extends BaseDialogFragment {
    private String mSelectedCountry;
    private OnCountrySelectListener mOnCountrySelectListener;

    public static StateSelectDialog newInstance(String selectedCountry, OnCountrySelectListener listener) {
        StateSelectDialog languageSelectDialog = new StateSelectDialog();
        languageSelectDialog.mSelectedCountry = selectedCountry;
        languageSelectDialog.mOnCountrySelectListener = listener;
        return languageSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_country;
    }

    @Override
    protected void setDialogContentView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.dialog_country_list);
        TextView textView = (TextView) view.findViewById(R.id.dialog_country_title);
        textView.setText(getString(R.string.person_area));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        final CountryAdapter countryAdapter = new CountryAdapter(getContext(), R.layout.item_dialog_select_country,
                null);
        countryAdapter.initSelectState(mSelectedCountry);
        recyclerView.setAdapter(countryAdapter);
        countryAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                int selectedPosition = countryAdapter.getSelectedPosition();
                if (selectedPosition != position) {
                    countryAdapter.setItemSelectState(selectedPosition, false);
                    countryAdapter.notifyItemChanged(selectedPosition);
                    countryAdapter.setSelectedPosition(position);
                    countryAdapter.setItemSelectState(position, true);
                    countryAdapter.notifyItemChanged(position);
                }
            }
        });
        Button btnCancel = (Button) view.findViewById(R.id.dialog_country_cancel);
        Button btnSave = (Button) view.findViewById(R.id.dialog_country_save);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != mOnCountrySelectListener) {
                    mOnCountrySelectListener.onSelected(countryAdapter.getSelectedItem());
                }
            }
        });
    }

    public static void show(FragmentManager fragmentManager, String selectedCountry, OnCountrySelectListener onLanguageSelectListener) {
        newInstance(selectedCountry, onLanguageSelectListener).show(fragmentManager, "country_select");
    }

    public interface OnCountrySelectListener {
        void onSelected(String country);
    }

}
