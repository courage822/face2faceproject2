package com.wiscomwis.facetoface.ui.pay.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.facetoface.ui.pay.adapter.DredgeVipAdapter;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface DredgeVipContract {

    interface IView extends BaseView {

        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();
        /**
         * 获取支付信息1
         */
        void setInfoString1(String serviceName1,String price1,String serviceId1,String des1);
        /**
         * 获取支付信息2
         */
        void setInfoString2(String serviceName2,String price2,String serviceId2,String des2);
        /**
         * 获取支付信息3
         */
        void setInfoString3(String serviceName3,String price3,String serviceId3,String des3);
        /**
         * 设置adapter
         */
        void setAdapter(DredgeVipAdapter adapter);
        /**
         * 获取滚动的字幕
         */
        void getGunDongText(List<String> title,List<String> content);
        /**
         * 判断注册是否已经过了24小时
         */
        void isPast24Hours();
        /**
         * 获取倒计时的时间
         */
         void getCutdownTime(int s);

    }

    interface IPresenter extends BasePresenter {

        /**
         * @return 从后台获取支付渠道信息
         */
        void getPayWay(String fromTag);
    }

}
