package com.wiscomwis.facetoface.ui.setting;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.AdviceEvent;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.ui.login.LoginActivity;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DeviceUtil;
import com.wiscomwis.library.util.FileUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.NotificationHelper;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public class SettingPresenter implements SettingContract.IPresenter {
    private SettingContract.IView mSettingView;
    private Context mContext;

    public SettingPresenter(SettingContract.IView view) {
        this.mSettingView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void getNoDistrubState() {
        mSettingView.toggleNoDistrub("3".equals(UserPreference.getStatus()));
    }

    @Override
    public void toggleNoDistrub(final boolean toggle) {
        if ((toggle && "3".equals(UserPreference.getStatus())) || (!toggle && !"3".equals(UserPreference.getStatus()))) {
            return;
        }
        ApiManager.modifyUserStatus(toggle ? "3" : "1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                UserPreference.setStatus(toggle ? "3" : "1");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void getCacheSize() {
        mSettingView.setCacheSize(FileUtil.getTotalCacheSize(mContext));
    }

    @Override
    public void clearCache() {
        mSettingView.showAlertDialog(mContext.getString(R.string.setting_clear_cache_tip), new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {
            }

            @Override
            public void onPositiveClick(View view) {
                // 清除系统缓存
                FileUtil.clearAllCache(mContext);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // 清除Glide图片磁盘缓存
                        ImageLoaderUtil.getInstance().clearDiskCache(mContext);
                    }
                }).start();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        // 清除Glide图片内存缓存
                        ImageLoaderUtil.getInstance().clearMemoryCache(mContext);
                        mSettingView.setCacheSize("0.0B");
                        mSettingView.showTip(mContext.getString(R.string.setting_clear_cache_finish));
                    }
                });
            }
        });
    }

    @Override
    public void getVersionName() {
        mSettingView.setVersion(DeviceUtil.getVersionName(mContext));
    }

    @Override
    public void logout() {
        mSettingView.showAlertDialog(mContext.getString(R.string.setting_logout_tip), new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {
            }

            @Override
            public void onPositiveClick(View view) {
                LaunchHelper.getInstance().launchFinish(mContext, LoginActivity.class);
                NotificationHelper.getInstance(mContext).cancelAll();//清空所有通知
                AgoraHelper.getInstance().logout();
                HyphenateHelper.getInstance().logout();
                EventBus.getDefault().post(new FinishEvent());
                DataPreference.saveYeJiaoJsonData(null);//保存透传的json数据
                switchUser();
            }
        });
    }

    @Override
    public void getAdviceAndContact(String advice, String contract) {
        if(!TextUtils.isEmpty(advice)&&!TextUtils.isEmpty(contract)){
          ApiManager.adviceForUs(advice, contract, new IGetDataListener<BaseModel>() {
              @Override
              public void onResult(BaseModel baseModel, boolean isEmpty) {
                  Toast.makeText(mContext, mContext.getString(R.string.advice_send_success), Toast.LENGTH_SHORT).show();
                     EventBus.getDefault().post(new AdviceEvent());
              }

              @Override
              public void onError(String msg, boolean isNetworkError) {

              }
          });
        }else{
            Toast.makeText(mContext, mContext.getString(R.string.not_null_content), Toast.LENGTH_SHORT).show();
        }
    }

    public static void switchUser(){
       ApiManager.switchUser(new IGetDataListener<BaseModel>() {
           @Override
           public void onResult(BaseModel baseModel, boolean isEmpty) {
           }

           @Override
           public void onError(String msg, boolean isNetworkError) {
           }
       });
   }
}
