package com.wiscomwis.facetoface.ui.register.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface CompleteSelfhoodInfoContract {

    interface IView extends BaseView {
        /**
         * 获取女标签1
         */
        String getFemale1();
        /**
         * 获取女标签2
         */
        String getFemale2();
        /**
         * 获取女标签3
         */
        String getFemale3();
    }

    interface IPresenter extends BasePresenter {
        /**
         * 获取女用户标签并提交到服务器
         */
        void commitFemaleData();
        /**
         * 获取男用户标签并并提交到服务器
         */
        void commitMaleData(String tag1,String tag2,String tag3);
    }
}
