package com.wiscomwis.facetoface.ui.message.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/10.
 */
public interface MessageContract {

    interface IView extends BaseView {

        FragmentManager getManager();

        void setAdapter(PagerAdapter pagerAdapter);

    }

    interface IPresenter extends BasePresenter {
        void addTabs();
    }

}
