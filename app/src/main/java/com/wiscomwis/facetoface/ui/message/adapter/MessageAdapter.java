package com.wiscomwis.facetoface.ui.message.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wiscomwis.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class MessageAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragmentList;
    private List<String> mTabList;

    public MessageAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList = new ArrayList<>();
        mTabList = new ArrayList<>();
    }

    public void setData(List<Fragment> fragments, List<String> tabs) {
        if (!Utils.isListEmpty(fragments)) {
            this.mFragmentList = fragments;
        }
        if (!Utils.isListEmpty(tabs)) {
            this.mTabList = tabs;
        }
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return Utils.isListEmpty(mFragmentList) ? null : mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return Utils.isListEmpty(mFragmentList) ? 0 : mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Utils.isListEmpty(mTabList) ? null : mTabList.get(position);
    }

}
