package com.wiscomwis.facetoface.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/23.
 */
public class MainAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragmentList;

    public MainAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        mFragmentList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList == null ? null : mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList == null ? 0 : mFragmentList.size();
    }

    public Fragment getVideoListFragment() {
        return mFragmentList.get(2);
    }
}
