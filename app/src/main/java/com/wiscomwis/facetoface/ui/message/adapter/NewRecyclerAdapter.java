package com.wiscomwis.facetoface.ui.message.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.message.HelloPeopleActivity;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuzhaole on 2017/12/1.
 */

public class NewRecyclerAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private boolean mIsAnchor;
    private LayoutInflater inflater;
    private int count = 1;

    private List<HuanXinUser> priList;
    private List<HuanXinUser> norList;
    private List<HuanXinUser> helloList;

    private int TYPE_KEFU = 1;
    private int TYPE_PRIVATE = 3;
    private int TYPE_NORMAL = 4;
    private int TYPE_HELLO = 2;
    private int TYPE_PRIVATE_COUNT = 5;
    private int TYPE_NORMAL_TOP = 6;

    private List<HuanXinUser> tempPrivateList;

    private boolean isEditable = false;


    public NewRecyclerAdapter(Context context, boolean mIsAnchor) {
        this.mContext = context;
        this.mIsAnchor = mIsAnchor;
        inflater = LayoutInflater.from(mContext);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_KEFU) {
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeKefuHolder(inflate);
        } else if (viewType == TYPE_PRIVATE) {
            View inflate = inflater.inflate(R.layout.item_chat_video, parent, false);
            return new TypePrivateHolder(inflate);
        } else if (viewType == TYPE_HELLO) {//打招呼的人，主播界面
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeHelloHolder(inflate);
        } else if (viewType == TYPE_PRIVATE_COUNT) {
            View inflate = inflater.inflate(R.layout.item_private_count, parent, false);
            return new TypePriCountHolder(inflate);
        } else if (viewType == TYPE_NORMAL_TOP) {
            View inflate = inflater.inflate(R.layout.item_normal_top, parent, false);
            return new TypeNormalTopHolder(inflate);
        } else {
            View inflate = inflater.inflate(R.layout.item_chat_history, parent, false);
            return new TypeNormalHolder(inflate);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof TypeKefuHolder) {
            initKefu((TypeKefuHolder) holder, position);
        } else if (holder instanceof TypeHelloHolder) {
            initHello((TypeHelloHolder) holder, position);
        } else if (holder instanceof TypePrivateHolder) {
            initPrivate((TypePrivateHolder) holder, position);
        } else if (holder instanceof TypePriCountHolder) {
            initPriCount((TypePriCountHolder) holder, position);
        } else if (holder instanceof TypeNormalTopHolder) {
            initNormalTop((TypeNormalTopHolder) holder, position);
        } else {
            initNormal((TypeNormalHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (UserPreference.isAnchor()) {
            if (position == 0) {
                return TYPE_KEFU;
            } else if (position == 1) {
                return TYPE_NORMAL_TOP;
            } else if (position == 2) {
                return TYPE_HELLO;
            } else if (position == 3) {
                return TYPE_NORMAL_TOP;
            } else {
                return TYPE_NORMAL;
            }
        } else {
            if ((priList == null || priList.size() == 0)) {
                if ((norList == null || norList.size() == 0)) {
                    return TYPE_KEFU;
                } else {
                    if (position == 0) {
                        return TYPE_NORMAL_TOP;
                    } else {
                        return TYPE_NORMAL;
                    }
                }
            } else {
                if (position == 0) {
                    return TYPE_PRIVATE_COUNT;
                } else if (position <= priList.size()) {
                    return TYPE_PRIVATE;
                } else if (position == priList.size() + 1) {
                    return TYPE_NORMAL_TOP;
                } else {
                    return TYPE_NORMAL;
                }
            }
        }
    }

    public class TypeKefuHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView nickname, message, time, msgnum;

        public TypeKefuHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
        }
    }

    public class TypeHelloHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        ImageView checkbox;
        TextView nickname, message, time, msgnum;

        public TypeHelloHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

        }
    }

    public class TypeNormalTopHolder extends RecyclerView.ViewHolder {

        public TypeNormalTopHolder(View itemView) {
            super(itemView);
        }
    }

    public class TypePrivateHolder extends RecyclerView.ViewHolder {
        ImageView avatar, iv_type;
        TextView nickname, message, time, msgnum;
        ImageView checkbox;

        public TypePrivateHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);
            iv_type = (ImageView) itemView.findViewById(R.id.iv_type);

        }
    }

    public class TypePriCountHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_video;

        public TypePriCountHolder(View itemView) {
            super(itemView);
            rl_video = (RelativeLayout) itemView.findViewById(R.id.rl_video);
        }
    }


    public class TypeNormalHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView nickname, message, time, msgnum;
        ImageView checkbox;

        public TypeNormalHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.item_chat_history_avatar);
            nickname = (TextView) itemView.findViewById(R.id.item_chat_history_nickname);
            message = (TextView) itemView.findViewById(R.id.item_chat_history_message);
            time = (TextView) itemView.findViewById(R.id.item_chat_is_tv_time);
            msgnum = (TextView) itemView.findViewById(R.id.item_chat_history_tv_msgnum);
            checkbox = (ImageView) itemView.findViewById(R.id.checkbox);
        }
    }

    private void initPriCount(TypePriCountHolder holder, int position) {
        holder.rl_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (priList != null && priList.size() > 0) {
                    if (tempPrivateList == null || tempPrivateList.size() == 0) {
                        return;
                    }
                    if (priList.size() > 1) {
                        priList.clear();
                        count -= tempPrivateList.size();
                        HuanXinUser huanXinUser = tempPrivateList.get(0);
                        priList.add(huanXinUser);
                        count += 1;
                        notifyDataSetChanged();
                    } else {
                        priList.clear();
                        count -= 1;
                        priList.addAll(tempPrivateList);
                        count += tempPrivateList.size();
                        notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void initHello(final TypeHelloHolder holder, int position) {
        holder.avatar.setImageResource(R.drawable.hi_icon);
        holder.nickname.setText(mContext.getString(R.string.hello_people));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, HelloPeopleActivity.class);
            }
        });
        if (helloList == null || helloList.size() == 0) {
            holder.msgnum.setVisibility(View.GONE);
        } else {
            if (helloList.size() >= 1) {
                holder.message.setText(mContext.getString(R.string.have) + helloList.size() + mContext.getString(R.string.hello_people_count));
                holder.msgnum.setVisibility(View.VISIBLE);
                int helloCount = 0;
                for (HuanXinUser user : helloList) {
                    helloCount += user.getMsgNum();
                }
                if (helloCount > 0) {
                    holder.msgnum.setText(helloCount + "");
                } else {
                    holder.msgnum.setVisibility(View.GONE);
                }
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
        }
        final HuanXinUser user;
        if (helloList != null && helloList.size() != 0) {
            user = helloList.get(position - 2);
            if (user != null) {
                if (isEditable) {
                    holder.checkbox.setVisibility(View.VISIBLE);
                    if (user.isSelect()) {
                        holder.checkbox.setImageResource(R.drawable.message_check);
                    } else {
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                    }
                } else {
                    holder.checkbox.setVisibility(View.GONE);
                }
                holder.checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user.isSelect()) {
                            user.setSelect(false);
                            holder.checkbox.setImageResource(R.drawable.message_uncheck);
                            mOnSelectStateListener.removeHxId(user.getHxId());
                        } else {
                            user.setSelect(true);
                            holder.checkbox.setImageResource(R.drawable.message_check);
                            mOnSelectStateListener.addHxId(user.getHxId());
                        }
                    }
                });
            }
        }
    }


    private void initKefu(TypeKefuHolder holder, final int position) {
        holder.nickname.setText(mContext.getString(R.string.kefu_desc));
        final HuanXinUser user = DbModle.getInstance().getUserAccountDao().getAccountByHyID("10000");
        if (user != null) {
            DataPreference.setSystemIcon(user.getHxIcon());
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
            holder.message.setText(user.getLastMsg());
        } else {
            holder.msgnum.setVisibility(View.GONE);
            holder.message.setText(mContext.getString(R.string.user_hello));
        }
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(DataPreference.getSystemIcon())
                .placeHolder(R.drawable.chat_admin).error(R.drawable.chat_admin).imageView(holder.avatar).build());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
            }
        });
    }

    private void initPrivate(final TypePrivateHolder holder, final int position) {
        if (priList == null || priList.size() == 0) {
            return;
        }
        final HuanXinUser user = priList.get(position - 1);
        if (user != null) {
            holder.nickname.setText(user.getHxName());
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(holder.avatar).build());
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }

            if (user.getExtendType() == 2) {//视频邀请
                holder.message.setText(mContext.getString(R.string.video_type));
                holder.iv_type.setImageResource(R.drawable.video_recall);
            } else if (user.getExtendType() == 4) {//语音邀请
                holder.message.setText(mContext.getString(R.string.voice_type));
                holder.iv_type.setImageResource(R.drawable.phone_recall);
            } else if (user.getExtendType() == 3) {//私密消息
                if (user.getMsgNum() > 0) {
                    holder.message.setText(user.getMsgNum() + mContext.getString(R.string.private_message_number));
                }
                holder.time.setText(TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));
            }

            if (isEditable) {
                holder.checkbox.setVisibility(View.VISIBLE);
                if (user.isSelect()) {
                    holder.checkbox.setImageResource(R.drawable.message_check);
                } else {
                    holder.checkbox.setImageResource(R.drawable.message_uncheck);
                }
            } else {
                holder.checkbox.setVisibility(View.GONE);
            }
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.isSelect()) {
                        user.setSelect(false);
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                        mOnSelectStateListener.removeHxId(user.getHxId());
                    } else {
                        user.setSelect(true);
                        holder.checkbox.setImageResource(R.drawable.message_check);
                        mOnSelectStateListener.addHxId(user.getHxId());

                    }
                }
            });

            holder.avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(user.getHxId()));
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    DbModle.getInstance().getUserAccountDao().setState(user, true);//把标记设置为已经读取
                    String hxId = user.getHxId();
                    if (!TextUtils.isEmpty(hxId)) {
                        long guid = Long.parseLong(hxId);
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                user.getAccount(), user.getHxName(), user.getHxIcon(), 0));
                    }
                }
            });

        }
    }

    private void initNormalTop(TypeNormalTopHolder holder, int position) {

    }

    private void initNormal(final TypeNormalHolder holder, final int position) {
        if (norList == null || norList.size() == 0) {
            return;
        }
        final HuanXinUser user;
        if (UserPreference.isAnchor()) {
            user = norList.get(position - 4);
        } else {
            if (priList == null || priList.size() == 0) {
                user = norList.get(position - 1);
            } else {
                user = norList.get(position - priList.size() - 2);
            }
        }
        if (user != null) {
            if (user.getHxId().equals("10000")) {
                holder.nickname.setText(mContext.getString(R.string.kefu_desc));
                DataPreference.setSystemIcon(user.getHxIcon());
            } else {
                holder.nickname.setText(user.getHxName());
            }
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(holder.avatar).build());
            if (user.getLastMsg().contains("picIcon")) {
                holder.message.setText(mContext.getResources().getString(R.string.gift));
            } else {
                holder.message.setText(String.valueOf(user.getLastMsg()));
            }
            if (user.getMsgNum() > 0) {
                holder.msgnum.setVisibility(View.VISIBLE);
                holder.msgnum.setText(user.getMsgNum() + "");
            } else {
                holder.msgnum.setVisibility(View.GONE);
            }
            holder.time.setText(TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));

            if (isEditable) {
                if (!user.getHxId().equals("10000")) {
                    holder.checkbox.setVisibility(View.VISIBLE);
                    if (user.isSelect()) {
                        holder.checkbox.setImageResource(R.drawable.message_check);
                    } else {
                        holder.checkbox.setImageResource(R.drawable.message_uncheck);
                    }
                } else {
                    holder.checkbox.setVisibility(View.GONE);
                }
            } else {
                holder.checkbox.setVisibility(View.GONE);
            }
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.getHxId().equals("10000")) {
                        ToastUtil.showShortToast(mContext, "不能删除客服");
                    } else {
                        if (user.isSelect()) {
                            user.setSelect(false);
                            holder.checkbox.setImageResource(R.drawable.message_uncheck);
                            mOnSelectStateListener.removeHxId(user.getHxId());
                        } else {
                            user.setSelect(true);
                            holder.checkbox.setImageResource(R.drawable.message_check);
                            mOnSelectStateListener.addHxId(user.getHxId());
                        }
                    }
                }
            });

            holder.avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user.getHxId().equals("10000")){
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                                "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));

                    }else{
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(user.getHxId()));
                    }
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    DbModle.getInstance().getUserAccountDao().setState(user, true);//把标记设置为已经读取
                    String hxId = user.getHxId();
                    if (!TextUtils.isEmpty(hxId)) {
                        long guid = Long.parseLong(hxId);
                        if (guid == 10000) {
                            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                                    "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));

                        } else {
                            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                    user.getAccount(), user.getHxName(), user.getHxIcon(), 0));
                        }
                    }
                }
            });
        }
    }

    public void setPrivateList(List<HuanXinUser> list) {
        priList = list;
        tempPrivateList = new ArrayList<>();
        tempPrivateList.addAll(list);
        if (tempPrivateList != null && tempPrivateList.size() > 0) {
            count += (tempPrivateList.size() + 1);
        }
        notifyDataSetChanged();
    }

    public void setNormalList(List<HuanXinUser> list) {
        norList = list;
        if (norList != null && norList.size() > 0) {
            if (UserPreference.isAnchor()) {
                count += (norList.size() + 1);
            } else {
                if (priList == null || priList.size() == 0) {
                    count += norList.size();
                } else {
                    count += (norList.size() + 1);
                }
            }
        }
        notifyDataSetChanged();
    }

    //打招呼
    public void setHellolList(List<HuanXinUser> list) {
        helloList = list;
        count += 2;
        notifyDataSetChanged();
    }


    public void reSet() {
        if (priList != null) {
            priList.clear();
            priList = null;
        }
        if (norList != null) {
            norList.clear();
            norList = null;
        }
        if (helloList != null) {
            helloList.clear();
            helloList = null;
        }
        count = 1;
        notifyDataSetChanged();
    }


    //设置编辑状态
    public void setEditable(boolean b) {
        this.isEditable = b;
        notifyDataSetChanged();
    }

    private OnSelectStateListener mOnSelectStateListener;

    public void setOnSelectStateListener(OnSelectStateListener mOnSelectStateListener) {
        this.mOnSelectStateListener = mOnSelectStateListener;
    }

    public interface OnSelectStateListener {
        void addHxId(String hxid);

        void removeHxId(String hxid);
    }

}
