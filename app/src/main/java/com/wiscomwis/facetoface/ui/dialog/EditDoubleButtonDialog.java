package com.wiscomwis.facetoface.ui.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.library.dialog.BaseDialogFragment;


/**
 * 两个按钮的对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class EditDoubleButtonDialog extends BaseDialogFragment {
    private OnEditDoubleDialogClickListener mOnDialogClickListener;
    private EditText tvMessage;

    public static EditDoubleButtonDialog newInstance(String title, String message, String positive, String negative, boolean isCancelable, OnEditDoubleDialogClickListener listener) {
        EditDoubleButtonDialog doubleButtonDialog = new EditDoubleButtonDialog();
        doubleButtonDialog.mOnDialogClickListener = listener;
        doubleButtonDialog.setArguments(getDialogBundle(title, message, positive, negative, isCancelable));
        return doubleButtonDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_edit_double_btn;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_two_btn_title);
        View dividerView = view.findViewById(R.id.dialog_two_btn_divider);// 标题栏下分割线
        tvMessage = (EditText) view.findViewById(R.id.dialog_two_btn_content);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_two_btn_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_two_btn_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String message = getDialogMessage();
        if (!TextUtils.isEmpty((message))) {
            tvMessage.setHint(message);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v, tvMessage.getText().toString());
                }
                dismiss();
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onNegativeClick(v);
                }
                dismiss();
            }
        });
    }

}
