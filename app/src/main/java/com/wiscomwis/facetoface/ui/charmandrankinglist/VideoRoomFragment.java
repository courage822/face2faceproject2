package com.wiscomwis.facetoface.ui.charmandrankinglist;

import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CheckStatus;
import com.wiscomwis.facetoface.data.model.VideoSquare;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.GiftSendEvent;
import com.wiscomwis.facetoface.event.IsFollow;
import com.wiscomwis.facetoface.event.PauseEvent;
import com.wiscomwis.facetoface.event.StartEvent;
import com.wiscomwis.facetoface.event.UpdataFollowUser;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.parcelable.VideoShowParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.facetoface.ui.personalcenter.AuthenticationActivity;
import com.wiscomwis.facetoface.ui.personalcenter.VideoShowActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.playerlibrary.EmptyControlVideo;
import com.wiscomwis.playerlibrary.gsyvideoplayer.GSYVideoManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 待整理，优化
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoRoomFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.player_empty_room)
    EmptyControlVideo player_empty_room;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.rl_avatar)
    RelativeLayout rlAvatar;
    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.iv_video)
    ImageView ivVideo;
    @BindView(R.id.rl_video)
    RelativeLayout rlVideo;
    @BindView(R.id.iv_send_gift)
    ImageView ivSendGift;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.iv_cover_photo)
    ImageView iv_cover_photo;
    @BindView(R.id.iv_follow)
    ImageView iv_follow;
    private VideoSquare mData;
    protected boolean isInit = false;
    protected boolean isLoad = false;
    private long lastClickTime = 0;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        initView();
    }

    private void playVideoShow() {
        player_empty_room.setUp(mData.gettUserVideoShow().getVideoUrl(), true, "123");
        player_empty_room.setLooping(true);
        player_empty_room.startPlayLogic();
    }

    private int getActivityPosition() {
        MainActivity mainActivity = (MainActivity) getActivity();
        return mainActivity.getPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && getActivityPosition() == 2) {
            playVideoShow();
        }
    }

    @Override
    public void onPause() {
        if (getUserVisibleHint() && getActivityPosition() == 2){
            GSYVideoManager.instance().onPause();
        }
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoad = false;
        isInit = false;
    }

    @Override
    protected void initViews() {
        isInit = true;
        initView();
    }

    protected void initView() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (mData != null && mData.getUserBase() != null) {
                tvName.setText(mData.getUserBase().getNickName());
                tvAccount.setText(mData.getUserBase().getOwnWords());
            }
            if (mData != null && mData.gettUserVideoShow() != null) {
                if (mData.getIsFollow() == 1) {
                    iv_follow.setVisibility(View.GONE);
                } else {
                    iv_follow.setVisibility(View.VISIBLE);
                }
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                        .url(mData.getUserBase().getIconUrlMininum())
                        .transform(new CropCircleTransformation(mContext))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());

                playVideoShow();
                isLoad = true;
            }
        }
    }


    @Override
    protected void setListeners() {
        rlAvatar.setOnClickListener(this);
        rlVideo.setOnClickListener(this);
        llChat.setOnClickListener(this);
        ivSendGift.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_show;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    public void setData(VideoSquare t) {
        this.mData = t;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_avatar://头像
                if (mData == null) {
                    return;
                }
                if (UserPreference.isAnchor()) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                    return;
                }
                if (mData.getIsFollow() == 1) {//已关注，进入到详情
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(mData.getUserBase().getGuid())));
                } else {//未关注，直接关注
                    follow();
                }
                break;
            case R.id.rl_video://视频
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    goToVideoInvite();
                    if(UserPreference.isVip()){//付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","12");
                    }else{//未付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","12");
                    }
                }

                break;
            case R.id.ll_chat://聊天
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }

                    //跳转写信的界面
                    goToWriteMessage();
                    if(UserPreference.isVip()){//付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","12");
                    }else{//未付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","14");
                    }
                }
                break;
            case R.id.iv_send_gift://送礼物
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    CustomDialogAboutOther.giveGiftShow(mContext, mData.getUserBase().getGuid() + "", mData.getUserBase().getAccount(), mData.getUserBase().getIconUrlMininum(), mData.getUserBase().getNickName(), 1, false,2);
              if(UserPreference.isVip()){//付费
                  getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","14");
              }else{//未付费
                  getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","14");
              }

                }
                break;
        }
    }

    private void follow() {
        ApiManager.follow(mData.getUserBase().getGuid() + "", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new IsFollow(mData.getUserBase().getAccount()));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.follow_fail));
            }
        });
    }

    private void goToVideoInvite() {
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            getStatus();
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (mData.getUserBase() != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mData.getUserBase().getGuid(), mData.getUserBase().getAccount()
                            , mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    private void getStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    if (checkStatus.getAuditStatus() == 0) {//审核中
                        if (checkStatus.getShowStatus() == 1) {//完整
                            Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                        } else if (checkStatus.getShowStatus() == -1) {//不完整
                            AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                    "去认证", "取消", new OnDialogClickListener() {
                                        @Override
                                        public void onNegativeClick(View view) {
                                        }

                                        @Override
                                        public void onPositiveClick(View view) {
                                            LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                        }
                                    }
                            );
                        }
                    }else{
                        AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                "去认证", "取消", new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                    }
                                }
                        );
                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    private void goToWriteMessage() {
        if (mData.getUserBase() != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                        mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                getStatus();
            } else {
                if (UserPreference.isVip() || UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
                } else {
                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,7));

                }
            }
        }
    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        if (follow.getAccount().equals(mData.getUserBase().getAccount())) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(UpdataFollowUser follow) {
        if (follow.getGuid().equals(String.valueOf(mData.getUserBase().getGuid()))) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(GiftSendEvent giftSendEvent) {
        if (getUserVisibleHint() &&  player_empty_room.isInPlayingState()) {
            goToWriteMessage();
        }
    }
    @Subscribe
    public void onEvent(StartEvent start) {
        if (getUserVisibleHint()) {
            GSYVideoManager.instance().onResume();
        }
    }

    @Subscribe
    public void onEvent(PauseEvent pause) {
        if (player_empty_room != null) {
            GSYVideoManager.instance().onPause();
        }
    }
    //为点击事件埋点
    private void getClickNum(String remoteId,String extendTag,String baseTag){
        ApiManager.userActivityTag(UserPreference.getId(), remoteId, baseTag, extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
