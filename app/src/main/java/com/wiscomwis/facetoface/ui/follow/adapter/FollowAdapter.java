package com.wiscomwis.facetoface.ui.follow.adapter;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.event.UpdataFollowUser;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2017/6/14.
 *
 */

public class FollowAdapter extends CommonRecyclerViewAdapter<UserBase> {
    private FragmentManager mFragmentManager;
    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public FollowAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }
    @Override
    public void convert(final UserBase userBase, int position, RecyclerViewHolder holder) {
        ImageView iv_avatar = (ImageView) holder.getView(R.id.follow_item_iv_avatar);
        TextView tv_sayhello = (TextView) holder.getView(R.id.follow_item_tv_sayhello);
        if (userBase!=null) {
            holder.setText(R.id.follow_item_tv_nickname,userBase.getNickName());
            holder.setText(R.id.follow_item_tv_age,userBase.getAge()+mContext.getString(R.string.edit_info_years_old));

            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
            String status = userBase.getStatus();
            if(status.equals("1")){
                holder.setText(R.id.follow_item_tv_state,mContext.getString(R.string.not_online));
            }else{
                holder.setText(R.id.follow_item_tv_state,mContext.getString(R.string.edit_info_status));
            }
            tv_sayhello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    unFollowUser(String.valueOf(userBase.getGuid()));
                }
            });
        }
    }
    public void unFollowUser(final String userId){
        ApiManager.unFollow(userId,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new UpdataFollowUser(userId));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    };

}
