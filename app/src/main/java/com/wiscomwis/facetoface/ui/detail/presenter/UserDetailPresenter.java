package com.wiscomwis.facetoface.ui.detail.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.common.ParamsUtils;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CheckStatus;
import com.wiscomwis.facetoface.data.model.HostInfo;
import com.wiscomwis.facetoface.data.model.TUserVideoShow;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.model.UserDetailforOther;
import com.wiscomwis.facetoface.data.model.UserMend;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.facetoface.data.model.UserTag;
import com.wiscomwis.facetoface.data.model.VideoOrImage;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.SayHelloEvent;
import com.wiscomwis.facetoface.parcelable.BigPhotoParcelable;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.parcelable.VideoShowParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.contract.UserDetailContract;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.facetoface.ui.personalcenter.AuthenticationActivity;
import com.wiscomwis.facetoface.ui.personalcenter.VideoShowActivity;
import com.wiscomwis.facetoface.ui.photo.BigPhotoActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public class UserDetailPresenter implements UserDetailContract.IPresenter {
    private UserDetailContract.IView mUserDetailView;
    private Context mContext;
    private UserBase userBase;
    private int pos=0;//主播的审核状态判断
    private int showStatus=0;
    public UserDetailPresenter(UserDetailContract.IView mUserDetailView) {
        this.mUserDetailView = mUserDetailView;
        this.mContext = mUserDetailView.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void getUserInfoData() {
        mUserDetailView.showLoading();
        mUserDetailView.meIsAuthor(UserPreference.isAnchor());
        ApiManager.getUserInfo(mUserDetailView.getUserId(), new IGetDataListener<UserDetailforOther>() {
            @Override
            public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                if (userDetailforOther != null) {
                    UserDetail userDetail = userDetailforOther.getUserDetail();
                    if (userDetail != null) {
                        List<VideoOrImage> list = new ArrayList<VideoOrImage>();
                        List<TUserVideoShow> userVideoShows = userDetail.getUserVideoShows();
                        if (userVideoShows != null && userVideoShows.size() != 0) {
                            TUserVideoShow tUserVideoShow = userVideoShows.get(0);
                            list.add(new VideoOrImage(tUserVideoShow.getThumbnailUrl(), true, tUserVideoShow.getVideoUrl()));
                        }
                        UserBase userBase1 = userDetail.getUserBase();
                        String vipDays = userDetail.getVipDays();
                        if(!TextUtils.isEmpty(vipDays)&&vipDays.length()>0){
                            int i = Integer.parseInt(vipDays);
                            if (i>0||i==-1) {
                            mUserDetailView.isHaveVip();
                            }
                        }
                        if (userBase1 != null) {
                            userBase = userBase1;
                            list.add(new VideoOrImage(userBase1.getIconUrlMiddle(), false, null));
                            mUserDetailView.setNickName(userBase1.getNickName());
                            mUserDetailView.setAge(userBase1.getAge() + "岁");
                            mUserDetailView.setSign(ParamsUtils.getSignValue(String.valueOf(userBase1.getSign())));
                            mUserDetailView.setUserId(userBase1.getAccount());
                            mUserDetailView.setAvatar(userBase1.getIconUrlMininum());

                            int userType = userBase1.getUserType();
                            if (userType == 1) {
                                mUserDetailView.isAnchor();
                                int gender = userBase1.getGender();
                                if (gender == 0) {
                                    mUserDetailView.setSex("男");
                                } else {
                                    mUserDetailView.setSex("女");
                                }
                            } else {
                                int gender = userBase1.getGender();
                                if (gender == 0) {
                                    mUserDetailView.setSex("男");
                                    mUserDetailView.isMail(true);
                                } else {
                                    mUserDetailView.setSex("女");
                                    if (userBase1.getUserType() == 0) {
                                        mUserDetailView.isMail(false);
                                    }
                                }
                            }
                        }


                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            for (UserPhoto userPhoto : userPhotos) {
                                list.add(new VideoOrImage(userPhoto.getFileUrlMiddle(), false, null));
                            }
                        }
                        mUserDetailView.startBanner(list);
                        int isSayHello = userDetail.getIsSayHello();
                        if (isSayHello == 1) {
                            mUserDetailView.isSayHello();
                        }
                        UserMend userMend = userDetail.getUserMend();
                        if (userMend != null) {
                            String isLogin = userMend.getIsLogin();
                            if (!TextUtils.isEmpty(isLogin) && isLogin.length() > 0) {
                                int status = Integer.parseInt(isLogin);
                                switch (status) {
                                    case 1:
                                        mUserDetailView.getStatus("在线");
                                        break;
                                    case -1:
                                        mUserDetailView.getStatus("不在线");
                                        break;
                                }

                            }
                            if (!TextUtils.isEmpty(userMend.getHeight())) {
                                mUserDetailView.setHeight(userMend.getHeight() + "cm");
                            } else {
                                mUserDetailView.setHeight("173cm");
                            }

                            if (!TextUtils.isEmpty(userMend.getRelationshipId()) && !userMend.getRelationshipId().equals("31")) {
                                mUserDetailView.setMerital_status(DataPreference.getValueByKey(userMend.getRelationshipId(), 1));
                            } else {
                                mUserDetailView.setMerital_status("保密");
                            }

                            if (!TextUtils.isEmpty(userMend.getWeight())) {
                                mUserDetailView.setWeight(userMend.getWeight() + "kg");
                            } else {
                                mUserDetailView.setWeight("保密");

                            }
                        }

                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            int price = (int) hostInfo.getPrice();
                            mUserDetailView.setAnchorPrice(price);
                        } else {
                            mUserDetailView.setAnchorPrice(0);

                        }
                        mUserDetailView.getFollowOrUnFollow(userDetail);
                        List<UserTag> userTags = userDetail.getUserTags();
                        if(userTags!=null){
                            if(userTags.size()==3){
                                UserTag userTag1 = userTags.get(0);
                                UserTag userTag2 = userTags.get(1);
                                UserTag userTag3 = userTags.get(2);
                                mUserDetailView.setTag(userTag1.getTagName(),userTag2.getTagName(),userTag3.getTagName());
                                mUserDetailView.haveVip();
                            }else{
                                mUserDetailView.isHaveTag();
                            }
                        }else{
                            mUserDetailView.isHaveTag();
                        }
                    }
                }
                mUserDetailView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (mUserDetailView != null) {
//                    mUserDetailView.dismissLoading();
                }
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
//                    mUserDetailView.showTip(mContext.getString(R.string.load_fail));
                }
            }
        });
    }

    //关注
    @Override
    public void follow(String remoteUid) {
        ApiManager.follow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    //   mUserDetailView.showTip(msg);
                }
            }
        });
    }

    //取消关注
    @Override
    public void unFollow(String remoteUid) {
        ApiManager.unFollow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }


    @Override
    public void goVideoCallPage() {
        if (userBase != null) {
            if (UserPreference.isAnchor() && userBase.getUserType() == 1) {
//                mUserDetailView.innerAnchor(mContext.getString(R.string.user_detail_other_is_author));
                //跳转视频聊天的界面
                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                        , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
            } else {
                if (!TextUtils.isEmpty(userBase.getStatus())) {
                    //跳转视频聊天的界面
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void goWriteMessagePage() {
        //跳转写信的界面
        if (userBase != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                        userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                Log.e("AAAAAAA", "goWriteMessagePage: 111111==="+pos);
                if (pos == 0) {//审核中
                    if (showStatus == 1) {//完整
                        Log.e("AAAAAAA", "goWriteMessagePage: 2222=="+showStatus);
                        Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                    } else if (showStatus == -1) {//不完整
                        Log.e("AAAAAAA", "goWriteMessagePage: 333=="+showStatus);
                        AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "聊天交友要通过真人认证哦",
                                "去认证", "取消", new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }
                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                    }
                                }
                        );
                    }
                }else{
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "视频聊天需要通过真人认证哦",
                            "去认证", "取消", new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                }
                            }
                    );
                }
            } else {
                if (UserPreference.isVip() || UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                            userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
                } else {
                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,8));

                }
            }
        }
    }

    @Override
    public void goToAvatarViewPager() {
        List<String> list_photo = new ArrayList<>();
        if (userBase != null) {
            list_photo.add(userBase.getIconUrlMiddle());
        }
        if (list_photo != null && list_photo.size() > 0) {
            // 点击图片查看大图
            LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0,
                    list_photo));
        }
    }

    @Override
    public void sayHelloClick() {
        Toast.makeText(mContext, "打招呼成功", Toast.LENGTH_SHORT).show();
        mUserDetailView.isSayHello();
        if (userBase != null) {
            ApiManager.sayHello(String.valueOf(userBase.getGuid()), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel videoStop, boolean isEmpty) {
                    EventBus.getDefault().post(new SayHelloEvent());
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {

                }
            });
        }
    }

    @Override
    public void sendVideoInvite(String remoteId) {
        Log.e("AAAAAAA", "sendVideoInvite: 11111111111111111111111111");
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            Log.e("AAAAAAA", "sendVideoInvite: 222222222222222222222222");
            if (pos == 0) {//审核中
                if (showStatus == 1) {//完整
                    Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                } else if (showStatus == -1) {//不完整
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "视频聊天需要通过真人认证哦",
                            "去认证", "取消", new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                }
                            }
                    );
                }
            }else{
                AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "视频聊天需要通过真人认证哦",
                        "去认证", "取消", new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                            }
                        }
                );
            }
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, "您当前处于勿扰模式，请从设置中修改状态", Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void sendVoiceInvite(String remoteId) {//语音邀请
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            if (pos == 0) {//审核中
                if (showStatus == 1) {//完整
                    Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                } else if (showStatus == -1) {//不完整
                    AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "语音聊天需要通过真人认证哦",
                            "去认证", "取消", new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                }
                            }
                    );

                }
            }else{
                AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "", "视频聊天需要通过真人认证哦",
                        "去认证", "取消", new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                            }
                        }
                );
            }

        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, "您当前处于勿扰模式，请从设置中修改状态", Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 1, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void getCheckStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    Log.e("AAAAAAA", "onResult: "+checkStatus.toString());
                    pos=checkStatus.getAuditStatus();
                    showStatus=checkStatus.getShowStatus();
//                    mUserDetailView.getCheckStatus(checkStatus.getAuditStatus(),checkStatus.getShowStatus());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }


}
