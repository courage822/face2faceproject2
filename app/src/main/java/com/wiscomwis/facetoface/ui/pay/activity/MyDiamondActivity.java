package com.wiscomwis.facetoface.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.ui.pay.adapter.MyDiamondAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.MyDiamondContract;
import com.wiscomwis.facetoface.ui.pay.presenter.MyDiamondPresenter;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/830.
 */

public class MyDiamondActivity extends BaseAppCompatActivity implements View.OnClickListener,MyDiamondContract.IView{
    @BindView(R.id.mydiamond_activity_ll_root)
    LinearLayout mLlRoot;
    @BindView(R.id.mydiamond_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.mydiamond_activity_recyclerview)
    RecyclerView mRecyelerView;
    @BindView(R.id.mydiamond_activity_tv_dionmads)
    TextView tv_dionmads;
    private MyDiamondPresenter mMyDiamondPresenter;
    private PayParcelable mPayParcelable;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_mydiamond;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayParcelable= (PayParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyelerView.setLayoutManager(linearLayoutManager);
        mRecyelerView.setHasFixedSize(true);
        mRecyelerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mMyDiamondPresenter=new MyDiamondPresenter(this);
        mMyDiamondPresenter.getPayWay(C.pay.FROM_TAG_PERSON);
        if(TimeUtils.timeIsPast()){//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        }else if(!TimeUtils.timeIsPast()){
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
             switch (v.getId()){
                 case R.id.mydiamond_activity_rl_back:
                     finish();
                     break;
             }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mMyDiamondPresenter.start();
    }

    @Override
    public void setAdapter(MyDiamondAdapter adapter) {
        if (null != adapter) {
            if (mPayParcelable!=null) {
                adapter.setPayPresenter(mMyDiamondPresenter,mPayParcelable.paySource);
                mRecyelerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void getDionmads(String num) {
        tv_dionmads.setText(num);
    }
    @Subscribe
    public void onEvent(SingleLoginFinishEvent event){
        finish();//单点登录销毁的activity
    }
    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();

    }
}
