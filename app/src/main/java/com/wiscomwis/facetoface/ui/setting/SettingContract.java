package com.wiscomwis.facetoface.ui.setting;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.library.dialog.OnDialogClickListener;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface SettingContract {

    interface IView extends BaseView {

        /**
         * 开启/关闭免打扰模式
         */
        void toggleNoDistrub(boolean toggle);

        /**
         * 设置缓存大小
         *
         * @param cacheSize 当前系统缓存
         */
        void setCacheSize(String cacheSize);

        /**
         * 设置APP版本
         *
         * @param versionName 当前APP versionName
         */
        void setVersion(String versionName);

        /**
         * 显示提示对话框
         */
        void showAlertDialog(String message, OnDialogClickListener listener);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取免打扰模式状态
         */
        void getNoDistrubState();

        /**
         * 开关免打扰模式
         */
        void toggleNoDistrub(boolean toggle);

        /**
         * 获得缓存大小
         */
        void getCacheSize();

        /**
         * 清除缓存
         */
        void clearCache();

        /**
         * 获得VersionName
         */
        void getVersionName();

        /**
         * 登出
         */
        void logout();
        /**
         * 获取建议与联系方式
         */
        void getAdviceAndContact(String advice,String contract);
    }

}
