package com.wiscomwis.facetoface.ui;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.HostInfo;
import com.wiscomwis.facetoface.data.model.Login;
import com.wiscomwis.facetoface.data.model.PlatformInfo;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserBean;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.preference.AnchorPreference;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.PlatformPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.register.CompleteInfoActivity;
import com.wiscomwis.facetoface.ui.register.FirstPageActivity;
import com.wiscomwis.library.permission.PermissionCallback;
import com.wiscomwis.library.permission.PermissionManager;
import com.wiscomwis.library.permission.PermissonItem;
import com.wiscomwis.library.util.DeviceUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * 启动页面
 * Created by zhangdroid on 2017/5/12.
 */
public class SplashActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // 初始化平台信息
        initPlatformInfo();
        // 激活
        activation();

        checkPermission(this);
    }

    private void login() {
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            ApiManager.login(account, password, new IGetDataListener<Login>() {
                @Override
                public void onResult(Login login, boolean isEmpty) {
                    UserDetail userDetail = login.getUserDetail();
                    if (null != userDetail) {
                        // 更新用户相关信息
                        UserBase userBase = userDetail.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        UserBean userBean = userDetail.getUserBean();
                        if (null != userBean) {
                            BeanPreference.saveUserBean(userBean);
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (null != hostInfo) {
                            AnchorPreference.saveHostInfo(hostInfo);
                        }
                        if (userBase.getGender() == 0){//男士
                            if (userBase.getAge() == 0){

                                EventBus.getDefault().post(new FinishEvent());
                                LaunchHelper.getInstance().launchFinish(SplashActivity.this, CompleteInfoActivity.class);
                            }else{
                                ToastUtil.showShortToast(SplashActivity.this, login.getMsg());
                                launchMainActivity();
                                AgoraHelper.getInstance().login();
                            }
                        }else{//女士
                            ToastUtil.showShortToast(SplashActivity.this, login.getMsg());
                            launchMainActivity();
                            AgoraHelper.getInstance().login();
                        }
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    LaunchHelper.getInstance().launchFinish(SplashActivity.this, FirstPageActivity.class);//这个是跳转到登录界面
                }

            });
        } else {
            LaunchHelper.getInstance().launchFinish(SplashActivity.this, FirstPageActivity.class);//这个是跳转到注册界面
        }
    }

    private void launchMainActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LaunchHelper.getInstance().launchFinish(SplashActivity.this, MainActivity.class);
            }
        });
    }

    private void activation() {
        if (!UserPreference.isMactived()) {
            ApiManager.activation(new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    // 记录已经激活过
                    UserPreference.mActivation();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }

            });
        }
    }

    private void initPlatformInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setW(String.valueOf(DeviceUtil.getScreenWidth(this)));
        platformInfo.setH(String.valueOf(DeviceUtil.getScreenHeight(this)));
        platformInfo.setVersion(DeviceUtil.getVersionName(this));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        platformInfo.setPlatform("2");
        platformInfo.setProduct(C.PRODUCT_ID);
        platformInfo.setPid(getAndroidId());
        platformInfo.setImsi(getAndroidId());
        platformInfo.setCountry(Util.getLacalCountry());
        platformInfo.setLanguage(Util.getLacalLanguage());
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(String.valueOf(DeviceUtil.getVersionCode(this)));
        String lacalCountry = Util.getLacalCountry();
        if (!TextUtils.isEmpty(lacalCountry)) {
            if (lacalCountry.equals("China")) {
                platformInfo.setFid(C.CN_CESHI);
                UserPreference.setCountryId("171");
            }
//            else if(lacalCountry.equals("Taiwan")){
////                platformInfo.setFid(C.TW_FID);
////                UserPreference.setCountryId("174");
//            }
        }
        // 保存platform信息
        PlatformPreference.setPlatfromInfo(platformInfo);
        // 设置API公用参数
//        OkHttpHelper.getInstance().addCommonParam("platformInfo", PlatformPreference.getPlatformJsonString());

       //测量屏幕宽高像素
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels = dm.widthPixels;
        int heightPixels = dm.heightPixels;
        float density = dm.density;
        int screenWidth = (int) (widthPixels * density);
        int screenHeight = (int) (heightPixels * density);
        DataPreference.saveScreenWidth(screenWidth);
        DataPreference.saveScreenHeight(screenHeight);
    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getNetType() {
        String type = "0";
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static List<String> findDeniedPermissions(Activity activity, String... permission) {
        //存储没有授权的权限
        List<String> denyPermissions = new ArrayList<>();
        for (String value : permission) {
            if (ContextCompat.checkSelfPermission(activity, value) != PackageManager.PERMISSION_GRANTED) {
                //没有权限 就添加
                denyPermissions.add(value);
            }
        }
        return denyPermissions;
    }

    public void checkPermission(final Activity activity) {
        String[] permissions = {C.permission.PERMISSION_PHONE,C.permission.PERMISSION_WRITE_EXTERNAL_STORAGE,
                C.permission.PERMISSION_READ_EXTERNAL_STORAGE,C.permission.PERMISSION_CAMERA,C.permission.PERMISSION_RECORD_AUDIO};
        List<String> deniedPermissions = findDeniedPermissions(activity, permissions);
        if (deniedPermissions != null && deniedPermissions.size() > 0) {
            //大于0,表示有权限没申请

            // 请求所有高危权限
            PermissionManager.getInstance(this)
                    .addPermission(new PermissonItem(C.permission.PERMISSION_PHONE, getString(R.string.permission_phone), R.drawable.phone_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_WRITE_EXTERNAL_STORAGE, getString(R.string.permission_write_file), R.drawable.write_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_READ_EXTERNAL_STORAGE, getString(R.string.permission_read_file), R.drawable.read_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_CAMERA, getString(R.string.permission_camera), R.drawable.camera_permission))
                    .addPermission(new PermissonItem(C.permission.PERMISSION_RECORD_AUDIO, getString(R.string.permission_record), R.drawable.voice_permission))
                    .checkMutiPermission(new PermissionCallback() {
                        @Override
                        public void onGuaranteed(String permisson, int position) {
                        }

                        @Override
                        public void onDenied(String permisson, int position) {
                        }

                        @Override
                        public void onFinished() {
                            login();
                        }

                        @Override
                        public void onClosed() {
//                        login();

                            finish();
                        }
                    });
        } else {
            //拥有权限
            login();
        }
    }
}
