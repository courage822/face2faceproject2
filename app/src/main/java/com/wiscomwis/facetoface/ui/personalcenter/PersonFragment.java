package com.wiscomwis.facetoface.ui.personalcenter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarFragment;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.MessageArrive;
import com.wiscomwis.facetoface.event.PaySuccessEvent;
import com.wiscomwis.facetoface.event.RefreshEvent;
import com.wiscomwis.facetoface.event.UserInfoChangedEvent;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.parcelable.TvPriceParcelable;
import com.wiscomwis.facetoface.parcelable.VideoShowParcelable;
import com.wiscomwis.facetoface.ui.auto.AutoReplyActivity;
import com.wiscomwis.facetoface.ui.detail.EditInfoActivity;
import com.wiscomwis.facetoface.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.facetoface.ui.pay.activity.MyDiamondActivity;
import com.wiscomwis.facetoface.ui.pay.activity.MyWalletActivity;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.facetoface.ui.personalcenter.contract.PersonContract;
import com.wiscomwis.facetoface.ui.personalcenter.presenter.PersonPresenter;
import com.wiscomwis.facetoface.ui.photo.GetPhotoActivity;
import com.wiscomwis.facetoface.ui.setting.SettingActivity;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;

/**
 * 个人中心
 * Created by zhangdroid on 2017/5/23.
 */
public class PersonFragment extends BaseTopBarFragment implements View.OnClickListener, PersonContract.IView {
    // 用户信息
    @BindView(R.id.person_user_layout)
    RelativeLayout mRlUser;
    @BindView(R.id.person_user_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.user_avater_status)
    ImageView user_avater_status;
    @BindView(R.id.person_user_name)
    TextView mTvUserName;
    @BindView(R.id.person_user_id)
    TextView mTvUserId;
    @BindView(R.id.person_user_percent)
    TextView mTvUserPercent;// 接听率/投诉率
    // 账号信息
    @BindView(R.id.person_account_balance)
    TextView mTvBalance;
    @BindView(R.id.person_recharge)
    TextView mTvRecharge;// 充值
    @BindView(R.id.person_withdraw)
    TextView mTvWithdraw;// 提现
    // 价格设置（主播）
    @BindView(R.id.person_price_layout)
    RelativeLayout mRlPrice;
    // 设置
    @BindView(R.id.person_setting_layout)
    RelativeLayout mRlSetting;
    //再此以上是以前所写，一下是现在所写
    @BindView(R.id.person_fragment_ll_vip)
    LinearLayout ll_buy_vip;
    @BindView(R.id.person_fragment_ll_authentication)
    LinearLayout ll_authentication;
    @BindView(R.id.person_fragment_ll_person_user)
    LinearLayout ll_person_user;
    @BindView(R.id.person_fragment_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.person_fragment_rl_mywallet)
    RelativeLayout rl_mywallet;
    @BindView(R.id.person_fragment_rl_mydiamond)
    RelativeLayout rl_mydiamond;
    @BindView(R.id.person_fragment_rl_setting)
    RelativeLayout rl_setting;
    @BindView(R.id.person_fragment_center_rl_author)
    RelativeLayout rl_author;
    @BindView(R.id.person_fragment_view1)
    View view1;
    @BindView(R.id.person_fragment_view2)
    View view2;
    @BindView(R.id.person_fragment_view3)
    View view3;
    @BindView(R.id.person_fragment_iv_vip)
    ImageView iv_vip;
    @BindView(R.id.person_fragment_tv_renzheng)
    TextView tv_renzheng;
    @BindView(R.id.person_fragment_tv_kaitong)
    TextView tv_kaitong;
    @BindView(R.id.person_fragment_iv_right2)
    ImageView iv_right2;
    @BindView(R.id.person_fragment_ll_authentication2)
    LinearLayout ll_authentication2;
    @BindView(R.id.person_fragment_tv_renzheng2)
    TextView tv_renzheng2;
    @BindView(R.id.person_fragment_center_rl_author1)
    RelativeLayout rl_author1;
    @BindView(R.id.person_fragment_rl_myreceive)
    RelativeLayout rl_autoreceive;
    @BindView(R.id.person_fragment_rl_share)
    RelativeLayout rl_share;
    @BindView(R.id.person_fragment_view4)
    View view4;
    private PersonPresenter mPersonPresenter;
    private String pirce;
    private int audioPrice;
    private int isCheck = 1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_person;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.title_person);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void initViews() {
        mPersonPresenter = new PersonPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        mPersonPresenter.start();
        mPersonPresenter.loadPersonInfor();

    }

    @Override
    protected void setListeners() {
        mIvUserAvatar.setOnClickListener(this);
        mTvRecharge.setOnClickListener(this);
        mTvWithdraw.setOnClickListener(this);
        mRlPrice.setOnClickListener(this);
        mRlSetting.setOnClickListener(this);
        //再此以上是以前所写，一下是现在所写
        rl_setting.setOnClickListener(this);
        ll_authentication.setOnClickListener(this);
        ll_buy_vip.setOnClickListener(this);
        ll_person_user.setOnClickListener(this);
        rl_mydiamond.setOnClickListener(this);
        rl_mywallet.setOnClickListener(this);
        rl_author.setOnClickListener(this);
        ll_authentication2.setOnClickListener(this);
        rl_autoreceive.setOnClickListener(this);
        rl_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.person_fragment_ll_person_user:// 查看个人页
                LaunchHelper.getInstance().launch(mContext, EditInfoActivity.class);
                break;

            case R.id.person_fragment_ll_vip:// 充值
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0,6));
                break;

            case R.id.person_withdraw:// 提现
//                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class);
//                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class);
                break;

            case R.id.person_fragment_ll_authentication:// 主播认证
                if (isCheck == 0) {
                    Toast.makeText(mContext, "审核中......", Toast.LENGTH_SHORT).show();
                } else if (isCheck == -1) {
                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                } else {
                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                }
                break;
            case R.id.person_fragment_ll_authentication2:// 主播认证
                if (isCheck == 0) {
                    Toast.makeText(mContext, "审核中......", Toast.LENGTH_SHORT).show();
                } else if (isCheck == -1) {
                    LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                } else {
                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                }
                break;

            case R.id.person_price_layout:// 主播价格设置
                LaunchHelper.getInstance().launch(mContext, SetPriceActivity.class,
                        new TvPriceParcelable(pirce, audioPrice));
                break;
            case R.id.person_fragment_rl_setting:// 设置
                LaunchHelper.getInstance().launch(mContext, SettingActivity.class);
                break;
            case R.id.person_fragment_rl_mydiamond:
                LaunchHelper.getInstance().launch(mContext, MyDiamondActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0,6));
                break;

            case R.id.person_fragment_rl_mywallet:
                LaunchHelper.getInstance().launch(mContext, MyWalletActivity.class);
                break;
            case R.id.person_fragment_center_rl_author:
                break;
            case R.id.person_fragment_rl_myreceive:
                LaunchHelper.getInstance().launch(mContext, AutoReplyActivity.class);
                break;
            case R.id.person_user_avatar:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        mPersonPresenter.upLoadAvator(file, true);
                    }
                });
                break;
            case R.id.person_fragment_rl_share:
                LaunchHelper.getInstance().launch(mContext,ShareActivity.class);
                break;
        }
    }

    @Override
    protected void loadData() {
//        mPersonPresenter.getUserInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setUserAvatar(String url, String status) {
        if (!TextUtils.isEmpty(status)) {
            if (status.equals("1")) {
                user_avater_status.setVisibility(View.GONE);
            } else {
                user_avater_status.setVisibility(View.VISIBLE);
            }
        }
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvUserAvatar).build());
    }

    @Override
    public void setUserName(String name) {
        if (!TextUtils.isEmpty(name)) {
            mTvUserName.setText(name);
        }
    }

    @Override
    public void setUserId(String id) {
        if (!TextUtils.isEmpty(id)) {
            mTvUserId.setText(getString(R.string.person_id, id));
        }
    }

    @Override
    public void setUserPrecent(String receivePrecent, String complainPrecent) {
        if (!TextUtils.isEmpty(receivePrecent) && !TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(TextUtils.concat(getString(R.string.person_receive_precent, receivePrecent), "% \n",
                    getString(R.string.person_complain_precent, complainPrecent), "%"));
        } else if (!TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(getString(R.string.person_complain_precent, complainPrecent));
        }
    }

    @Override
    public void setBalance(float balance) {
        mTvBalance.setText("" + balance);
    }

    @Override
    public void setIsAnchor(boolean isAnchor) {
        mTvWithdraw.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        mTvRecharge.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        mRlPrice.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_author.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        rl_mydiamond.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        rl_author1.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        view4.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_autoreceive.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_mywallet.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        if (isAnchor) {
            ll_authentication2.setVisibility(View.GONE);
            EventBus.getDefault().post(new MessageArrive("0"));
        }
    }

    @Override
    public void setAccumulatedIncome(String income) {

    }

    @Override
    public void setAnchorCurrentIncome(String currentIncome) {
    }

    @Override
    public void setPrice(String price, int audioPrice1) {
        if (!TextUtils.isEmpty(price)) {
            pirce = price;
            audioPrice = audioPrice1;
        }
    }

    @Override
    public void vipDays(int days) {
        if (days > 0) {
            iv_vip.setBackgroundResource(R.drawable.f1_user_vip_nored);
            tv_kaitong.setTextColor(Color.parseColor("#c0c0c0"));
            tv_kaitong.setText(mContext.getString(R.string.remain) + days + mContext.getString(R.string.day));
        } else if (days < 0) {
            iv_vip.setBackgroundResource(R.drawable.f1_user_vip_nored);
            tv_kaitong.setTextColor(Color.parseColor("#c0c0c0"));
            tv_kaitong.setText("永久免费");
        } else {
            iv_vip.setBackgroundResource(R.drawable.f1_user_vip_nor);
        }
    }

    @Override
    public void setAdapter(AlbumPhotoAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void picGone() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void switchAllPay() {
        rl_author.setVisibility(View.GONE);
        mRlPrice.setVisibility(View.GONE);
        rl_mywallet.setVisibility(View.GONE);
        rl_mydiamond.setVisibility(View.GONE);
        iv_right2.setVisibility(View.VISIBLE);
        if (UserPreference.isAnchor()) {
            ll_authentication2.setVisibility(View.GONE);
            iv_right2.setVisibility(View.GONE);
            rl_author1.setVisibility(View.GONE);
        } else {
            ll_authentication2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void switchWallet() {
        rl_mywallet.setVisibility(View.GONE);
    }

    @Override
    public void switchDionmads() {
        rl_mydiamond.setVisibility(View.GONE);
    }

    @Override
    public void switchVip() {

    }

    @Override
    public void getCheckStatus(int pos, int showStatus) {
        if (pos == 0) {//审核中
            if (showStatus == 1) {//完整
                tv_renzheng.setText(mContext.getString(R.string.under_review));
                tv_renzheng2.setText(mContext.getString(R.string.under_review));
                isCheck = 0;
            } else if (showStatus == -1) {//不完整
                tv_renzheng.setText("认证不完整...");
                tv_renzheng2.setText("认证不完整...");
                isCheck = -1;
            }
        } else if (pos == -2||pos==-1) {
            tv_renzheng.setText("我的认证");
            tv_renzheng2.setText("我的认证");
            isCheck=1;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Subscribe
    public void onEvent(UserInfoChangedEvent event) {
        mPersonPresenter.getUserInfo();
    }

    @Subscribe
    public void onEvent(PaySuccessEvent event) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mPersonPresenter.getUserInfo();
        mPersonPresenter.getUploadInfo();
        mPersonPresenter.getCheckStatus();
    }

    @Subscribe
    public void onEvent(RefreshEvent event) {
        mPersonPresenter.getUserInfo();
        mPersonPresenter.getUploadInfo();
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void hideLoading() {
        toggleShowLoading(false, null);
    }
}
