package com.wiscomwis.facetoface.ui.register.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.UpLoadMyInfo;
import com.wiscomwis.facetoface.data.model.UpLoadMyPhoto;
import com.wiscomwis.facetoface.data.model.UploadInfoParams;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.UserInfoChangedEvent;
import com.wiscomwis.facetoface.ui.register.CompleteSelfhoodInfoActivity;
import com.wiscomwis.facetoface.ui.register.contract.CompleteInfoContract;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class CompleteInfoPresenter implements CompleteInfoContract.IPresenter {
    private CompleteInfoContract.IView mCompleteInfo;
    private Context mContext;

    public CompleteInfoPresenter(CompleteInfoContract.IView mCompleteInfo) {
        this.mCompleteInfo = mCompleteInfo;
        mContext = mCompleteInfo.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void completeInfo() {
        if (!UserPreference.isMale() && TextUtils.isEmpty(mCompleteInfo.getNickname())) {
            ToastUtil.showShortToast(mContext, mContext.getString(R.string.register_name_hint));
            return;
        }
        mCompleteInfo.showLoading();
        UploadInfoParams uploadInfoParams = new UploadInfoParams();
        if (!TextUtils.isEmpty(mCompleteInfo.getNickname())) {
            String name = mCompleteInfo.getNickname();
            uploadInfoParams.setNickName(name);
        } else {
            uploadInfoParams.setNickName(UserPreference.getNickname());
        }
        if (!TextUtils.isEmpty(mCompleteInfo.getAge())) {
            uploadInfoParams.setAge(mCompleteInfo.getAge());
        }
        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                LaunchHelper.getInstance().launchFinish(mContext, CompleteSelfhoodInfoActivity.class);
                // mCompleteInfo.dismissLoading();
                // 发送用户资料改变事件
                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mCompleteInfo.dismissLoading();
                if (isNetworkError) {
                    mCompleteInfo.showNetworkError();
                } else {
                    mCompleteInfo.showTip(msg);
                }
            }
        });
    }

    @Override
    public void upLoadAvator(File file, boolean flag) {
        ApiManager.upLoadMyPhotoOrAvator(file, flag, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                if (upLoadMyPhoto != null) {
                    UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
                            mCompleteInfo.setUserAvator(stringFile);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mCompleteInfo.setUserAvator(null);
                mCompleteInfo.canOnClick();
                if (isNetworkError) {
                    mCompleteInfo.showNetworkError();
                } else {
                    mCompleteInfo.showTip(msg);
                }
            }
        });
    }
    @Override
    public void loadInfoData() {
        ApiManager.getFaceBookAccount(new IGetDataListener<String>() {
            @Override
            public void onResult(String faceBookAccount, boolean isEmpty) {
                if (faceBookAccount != null) {
                    DataPreference.saveDataJson(faceBookAccount);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
