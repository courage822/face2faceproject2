package com.wiscomwis.facetoface.ui.follow.presenter;

import android.content.Context;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.FollowUser;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.follow.FindUserActivity;
import com.wiscomwis.facetoface.ui.follow.adapter.FollowAdapter;
import com.wiscomwis.facetoface.ui.follow.contract.FollowContract;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.Utils;

import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowPresenter implements FollowContract.IPresenter {
    private FollowContract.IView mFollowIview;
    private Context mContext;
    private FollowAdapter mFollowAdater ;
    private int pageNum = 1;
    private String pageSize="15";
    private List<UserBase> list;
    public FollowPresenter(FollowContract.IView mFollowIview) {
        this.mFollowIview = mFollowIview;
        this.mContext = mFollowIview.obtainContext();
    }


    @Override
    public void start() {

    }

    @Override
    public void  loadFollowUserList() {
        mFollowIview.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
    }

    public void initData() {
        mFollowAdater= new FollowAdapter(mContext, R.layout.follow_list_item);
        mFollowAdater.setFragmentManager(mFollowIview.obtainFragmentManager());
        mFollowAdater.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                UserBase itemByPosition = mFollowAdater.getItemByPosition(position);
                if (null != itemByPosition) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(itemByPosition.getGuid())));
                }
            }
        });
        // 设置加载更多
        mFollowIview.setAdapter(mFollowAdater, R.layout.common_load_more);
    }

    @Override
    public void refresh() {
        if(mFollowAdater!=null){
            mFollowAdater.clear();
            mFollowAdater=null;
        }
        if(list!=null&&list.size()>0){
            list.clear();
        }
        pageNum = 1;
        load();
    }


    @Override
    public void goToSearchPage() {
        LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
    }

    @Override
    public void initFirstData() {
        load();
    }

    private void load() {
        ApiManager.getFollowList(String.valueOf(pageNum),pageSize, new IGetDataListener<FollowUser>() {
            @Override
            public void onResult(FollowUser followUser, boolean isEmpty) {
                if(isEmpty){
                    if (pageNum == 1) {
                        mFollowIview.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mFollowIview.showNoMore();
                    }
                }else{
                    if (null != followUser) {
                       list = followUser.getUserBaseList();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                               initData();
                                mFollowAdater.clear();
                                mFollowAdater.bind(list);
                                mFollowIview.toggleShowEmpty(false, null);
                            } else if (pageNum > 1) {
                                mFollowAdater.appendToList(list);
                            }
                            mFollowIview.hideLoadMore();
                        }
                    }
                }
                mFollowIview.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mFollowIview.hideRefresh(1);
                if (!isNetworkError) {
                    mFollowIview.toggleShowError(true, msg);
                }
            }
        });
    }

    /**
     * 上拉加载更多
     */
    private void loadMore() {
       mFollowIview.showLoadMore();
        mFollowIview.toggleShowError(false, null);
        pageNum++;
        load();
    }
}
