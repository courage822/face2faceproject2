package com.wiscomwis.facetoface.ui.setting;

import android.os.Parcelable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.DeviceUtil;

import butterknife.BindView;

public class AbuoutUsActivity extends BaseFragmentActivity {
  @BindView(R.id.about_activity_tv_version)
   TextView tv_version;
    @BindView(R.id.about_activity_rl_back)
    RelativeLayout rl_back;



    @Override
    protected int getLayoutResId() {
        return R.layout.activity_abuout_us;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        tv_version.setText(getString(R.string.version_code)+ DeviceUtil.getVersionName(AbuoutUsActivity.this));
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
