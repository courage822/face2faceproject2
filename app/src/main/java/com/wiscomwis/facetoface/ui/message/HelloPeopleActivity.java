package com.wiscomwis.facetoface.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.event.MessageArrive;
import com.wiscomwis.facetoface.ui.auto.AutoReplyActivity;
import com.wiscomwis.facetoface.ui.message.contract.HelloPeopleContract;
import com.wiscomwis.facetoface.ui.message.presenter.HelloPeoplePresenter;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public class HelloPeopleActivity extends BaseFragmentActivity implements HelloPeopleContract.IView {
    @BindView(R.id.auto_reply_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.auto_reply_activity_tv_title)
    TextView tv_title;
    @BindView(R.id.auto_reply_activity_rl_add)
    RelativeLayout rl_add;
    @BindView(R.id.chat_history_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.chat_history_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    private HelloPeoplePresenter mHelloPeoplePresenter;
    private boolean mIsFirstLoad;


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_hello_people;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mHelloPeoplePresenter = new HelloPeoplePresenter(this);
        mHelloPeoplePresenter.start(mRecyclerView);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mHelloPeoplePresenter.refresh();
                }
            }
        });
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rl_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, AutoReplyActivity.class);
            }
        });
    }

    @Override
    protected void loadData() {
        mHelloPeoplePresenter.refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelloPeoplePresenter.refresh();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void setAdapter(RecyclerView.Adapter pagerAdapter) {
        mRecyclerView.setAdapter(pagerAdapter);
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelloPeoplePresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelloPeoplePresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Subscribe
    public void onEvent(MessageArrive arrive) {
        handler.sendEmptyMessage(1);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mHelloPeoplePresenter.refresh();
        }
    };
}
